/*
* PurchaseOrderDaoUnitTest.java 1.0 2016/9/12
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 12-Sep-2016        LongNH           Created
*/
package vn.fpt.fsoft.csms.dao;

import static org.assertj.core.api.Assertions.assertThatExceptionThrownBy;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.nullValue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import vn.fpt.fsoft.csms.configuration.RepositoryConfig;
import vn.fpt.fsoft.csms.entities.OrderType;
import vn.fpt.fsoft.csms.entities.PurchaseOrder;

/**
 * Test cases for all PurchaseOrderDao methods
 *
 * @author Nguyen Hoang Long
 * @version 1.0 12-Sep-2016
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(classes = RepositoryConfig.class)
@ActiveProfiles("test")
public class PurchaseOrderDaoUnitTest {

    @Autowired
    private PurchaseOrderDao PurchaseOrderDao;
    
    @Autowired
    private OrderTypeDao orderTypeDao;


    @Test
    public void getPurchaseOrderListTest() {
        List<PurchaseOrder> orders = PurchaseOrderDao.getPurchaseOrderList();
        assertThat(orders.size(), is(3));
    }

    @Test
    public void getPurchaseOrderTest() {
        PurchaseOrder order = PurchaseOrderDao.getPurchaseOrder("PO012");
        assertThat(order.getOrderNo(), is("PO012"));

        order = PurchaseOrderDao.getPurchaseOrder("PO069");
        assertThat(order, is(nullValue()));
    }

    @Test
    public void createPurchaseOrderTest() throws ParseException {
        final PurchaseOrder order = new PurchaseOrder();
        OrderType orderType = orderTypeDao.getOrderType("PO");
        order.setOrderNo("PO015");
        order.setOrderDate(new SimpleDateFormat("yyyy-MM-dd").parse("2016-01-01"));
        order.setOrderType(orderType);
        order.setDiscAmt(5.0);
        order.setTaxAmt(10.0);
        order.setTotalAmt(65000.0);

        PurchaseOrderDao.createPurchaseOrder(order);

        PurchaseOrder purchaseOrder = PurchaseOrderDao.getPurchaseOrder("PO015");

        assertThat(purchaseOrder.equals(order), is(true));

        // If TransId is null, JpaSystemException is thrown
        assertThatExceptionThrownBy(() -> {
            PurchaseOrder nullOrder = new PurchaseOrder();
            PurchaseOrderDao.createPurchaseOrder(nullOrder);
            return 0;
        }).isInstanceOf(JpaSystemException.class);
    }

    @Test
    public void deletePurchaseOrderTest() {
        PurchaseOrderDao.deletePurchaseOrder("PO012");
        PurchaseOrderDao.deletePurchaseOrder("PO013");
        PurchaseOrderDao.deletePurchaseOrder("PO014");
        
        assertThat(PurchaseOrderDao.getPurchaseOrderList().size(), is(0));

        assertThatExceptionThrownBy(() -> {
            PurchaseOrderDao.deletePurchaseOrder("PO015");
            return 0;
        }).isInstanceOf(InvalidDataAccessApiUsageException.class);
    }
}
