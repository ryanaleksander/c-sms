/*
* MoneyTransactionDaoUnitTest.java 1.0 2016/8/12
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 29-Aug-2016        AnhLT            Created
* 02-Sep-2016        AnhLT            Update test cases
* 03-Sep-2016        AnhLT            Add a new delete test case
*/
package vn.fpt.fsoft.csms.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import vn.fpt.fsoft.csms.configuration.RepositoryConfig;
import vn.fpt.fsoft.csms.entities.MoneyTransaction;
import static org.assertj.core.api.Assertions.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.nullValue;

/**
 * Test cases for all MoneyTransactionDao methods
 *
 * @author Lam Tuan Anh
 * @version 1.0 29-Aug-2016
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(classes = RepositoryConfig.class)
@ActiveProfiles("test")
public class MoneyTransactionDaoUnitTest {

    @Autowired
    private MoneyTransactionDao moneyTransactionDao;

    @Test
    public void getMoneyTransactionListTest() {
        List<MoneyTransaction> transactions = moneyTransactionDao.getMoneyTransactionList();
        assertThat(transactions.size(), is(3));
    }

    @Test
    public void getMoneyTransactionTest() {
        MoneyTransaction transaction = moneyTransactionDao.getMoneyTransaction("TR001");
        assertThat(transaction.getTransId(), is("TR001"));

        transaction = moneyTransactionDao.getMoneyTransaction("TR005");
        assertThat(transaction, is(nullValue()));
    }

    @Test
    public void createMoneyTransactionTest() throws ParseException {
        final MoneyTransaction transaction = new MoneyTransaction();
        transaction.setTransId("TR004");
        transaction.setTransAmt(100.0);
        transaction.setTransDate(new SimpleDateFormat("yyyy-MM-dd").parse("2016-01-01"));

        moneyTransactionDao.createMoneyTransaction(transaction);

        MoneyTransaction returnedTransaction = moneyTransactionDao.getMoneyTransaction("TR004");

        assertThat(returnedTransaction.equals(transaction), is(true));

        // If TransId is null, JpaSystemException is thrown
        assertThatExceptionThrownBy(() -> {
            MoneyTransaction nullTransaction = new MoneyTransaction();
            moneyTransactionDao.createMoneyTransaction(nullTransaction);
            return 0;
        }).isInstanceOf(JpaSystemException.class);
    }

    @Test
    public void deleteMoneyTransactionTest() {
        moneyTransactionDao.deleteMoneyTransaction("TR001");
        moneyTransactionDao.deleteMoneyTransaction("TR002");
        moneyTransactionDao.deleteMoneyTransaction("TR003");

        assertThat(moneyTransactionDao.getMoneyTransactionList().size(), is(0));

        assertThatExceptionThrownBy(() -> {
            moneyTransactionDao.deleteMoneyTransaction("TR004");
            return 0;
        }).isInstanceOf(InvalidDataAccessApiUsageException.class);
    }
}
