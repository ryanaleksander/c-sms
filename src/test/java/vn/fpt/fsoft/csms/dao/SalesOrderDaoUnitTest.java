/*
* SalesOrderDaoUnitTest.java 1.0 2016/9/2
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 2-Sep-2016        QuanLD            Created
*/
package vn.fpt.fsoft.csms.dao;

import static org.assertj.core.api.Assertions.assertThatExceptionThrownBy;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.nullValue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import vn.fpt.fsoft.csms.configuration.RepositoryConfig;
import vn.fpt.fsoft.csms.entities.Customer;
import vn.fpt.fsoft.csms.entities.InvoiceType;
import vn.fpt.fsoft.csms.entities.SalesOrder;
import vn.fpt.fsoft.csms.entities.SalesPerson;

/**
 * Test cases for all SalesOrderDao methods
 *
 * @author Le Duy Quan
 * @version 1.0 2-Sep-2016
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(classes = RepositoryConfig.class)
@ActiveProfiles("test")
public class SalesOrderDaoUnitTest {

    @Autowired
    private SalesOrderDao salesOrderDao;
    
    @Autowired
    private InvoiceTypeDao invoiceTypeDao;

    @Autowired
    private SalesPersonDao salesPersonDao;
    
    @Autowired
    private CustomerDao customerDao;


    @Test
    public void getSalesOrderListTest() {
        List<SalesOrder> orders = salesOrderDao.getSalesOrderList();
        assertThat(orders.size(), is(3));
    }

    @Test
    public void getSalesOrderTest() {
        SalesOrder order = salesOrderDao.getSalesOrder("SO031");
        assertThat(order.getOrderNo(), is("SO031"));

        order = salesOrderDao.getSalesOrder("SO042");
        assertThat(order, is(nullValue()));
    }

    @Test
    public void createSalesOrderTest() throws ParseException {
        final SalesOrder order = new SalesOrder();
        InvoiceType invoiceType = invoiceTypeDao.getInvoiceType("NM");
        SalesPerson salesPerson = salesPersonDao.getSalesPerson("SP2");
        Customer customerr = customerDao.getCustomer("Cust2");
        order.setOrderNo("SO034");
        order.setOrderDate(new SimpleDateFormat("yyyy-MM-dd").parse("2016-09-09"));
        order.setInvoiceType(invoiceType);
        order.setSalesPersonId(salesPerson);
        order.setCustomerId(customerr);
        order.setOrderDisc(5.0);
        order.setTaxAmt(10.0);
        order.setTotalAmt(65000.0);
        order.setDescription("Nhận 3 món hàng");

        salesOrderDao.createSalesOrder(order);

        SalesOrder returnedGoods = salesOrderDao.getSalesOrder("SO034");

        assertThat(returnedGoods.equals(order), is(true));

        // If order is null, NullPointerException is thrown
        assertThatExceptionThrownBy(() -> {
            SalesOrder nullOrder = new SalesOrder();
            salesOrderDao.createSalesOrder(nullOrder);
            return 0;
        }).isInstanceOf(NullPointerException.class);
    }

    @Test
    public void deleteSalesOrderTest() {
        salesOrderDao.deleteSalesOrder("SO031");
        salesOrderDao.deleteSalesOrder("SO032");
        salesOrderDao.deleteSalesOrder("SO033");
        
        assertThat(salesOrderDao.getSalesOrderList().size(), is(0));

        assertThatExceptionThrownBy(() -> {
            salesOrderDao.deleteSalesOrder("SO034");
            return 0;
        }).isInstanceOf(InvalidDataAccessApiUsageException.class);
    }
}
