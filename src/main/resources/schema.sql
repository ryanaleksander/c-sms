CREATE TABLE Customer (
	CustID nvarchar(20) NOT NULL,
	CustomerID nvarchar(50) NOT NULL,
	Address nvarchar(50),
	Phone nvarchar(20),
	Fax nvarchar(20),
	Email nvarchar(50),
	Overdue int NOT NULL,
	Amount decimal(18),
	OverdueAmt decimal(18),
	DueAmt decimal(18),
	Status nchar(2) NOT NULL,
	Description nvarchar(200),
	PRIMARY KEY (CustID)
);


CREATE TABLE Inventory (
	InvtID nvarchar(20) NOT NULL,
	InvtName nvarchar(50) NOT NULL,
	ClassName nvarchar(50),
	UnitID_T int,
	UnitID_L int NOT NULL,
	UnitRate int,
	SalesPriceT decimal(18),
	SalesPriceL decimal(18),
	QtyStock int,
	SlsTax decimal(18),
	Status nchar(2) NOT NULL,
	Description nvarchar(200),
	PRIMARY KEY (InvtID)
);

CREATE TABLE InvoiceType (
	InvoiceType nvarchar(2) NOT NULL,
	InvoiceName nvarchar(50),
	PRIMARY KEY (InvoiceType)
);

CREATE TABLE MoneyTransaction (
	TransID nvarchar(20) NOT NULL,
	TransDate date NOT NULL,
	TransAmt decimal(18,1) NOT NULL,
	Description nvarchar(200),
	PRIMARY KEY (TransID)
);

CREATE TABLE OrderType (
	OrderType nchar(2) NOT NULL,
	TypeName nvarchar(50) NOT NULL,
	PRIMARY KEY (OrderType)
);

CREATE TABLE Payment (
	PaymentID int NOT NULL,
	PaymentNo nvarchar(20) NOT NULL,
	PaymentDate date,
	PaymentAmt decimal(18),
	CustID nvarchar(20),
	SalesPersonID nvarchar(20),
	Description nvarchar(200),
	PRIMARY KEY (PaymentID)
);

CREATE TABLE PurchaseOrdDetail (
	OrderNo nvarchar(20) NOT NULL,
	InvtID nvarchar(20) NOT NULL,
	Qty int,
	PurchasePrice decimal(18),
	StockID nvarchar(20),
	QtyProm int,
	QtyPromAmt decimal(18),
	AmtProm decimal(18),
	TaxAmt decimal(18),
	Amount decimal(18),
	PRIMARY KEY (InvtID,OrderNo)
);

CREATE TABLE PurchaseOrder (
	OrderNo nvarchar(20) NOT NULL,
	OrderDate date,
	OrderType nchar(2) NOT NULL,
	OverdueDate date,
	DiscAmt decimal(18),
	PromAmt decimal(18),
	ComAmt decimal(18),
	TaxAmt decimal(18),
	TotalAmt decimal(18),
	PRIMARY KEY (OrderNo)
);

CREATE TABLE SalesOrder (
	OrderNo nvarchar(20) NOT NULL,
	OrderDate date NOT NULL,
	InvoiceType nvarchar(2) NOT NULL,
	CustID nvarchar(20) NOT NULL,
	OverdueDate date,
	OrderDisc decimal(18) NOT NULL,
	TaxAmt decimal(18),
	TotalAmt decimal(18),
	Payment decimal(18),
	Debt decimal(18),
	Description nvarchar(200),
	SalesPersonID nvarchar(20) NOT NULL,
	PRIMARY KEY (OrderNo)
);

CREATE TABLE SalesPerson (
	SalesPersonID nvarchar(20) NOT NULL,
	SalesPersonName nvarchar(50) NOT NULL,
	Address nvarchar(50),
	StockQuota int,
	Status nchar(2) NOT NULL,
	Description nvarchar(200),
	PRIMARY KEY (SalesPersonID)
);

CREATE TABLE SlsOrderDetail (
	ID int NOT NULL,
	OrderNo nvarchar(20) NOT NULL,
	InvtID nvarchar(20) NOT NULL,
	Qty int,
	SalesPrice decimal(18),
	Discount decimal(18),
	TaxAmt decimal(18),
	Amount decimal(18),
	PRIMARY KEY (ID)
);

CREATE TABLE Status (
  Status nchar(2) NOT NULL,
  StatusName nvarchar(50) NOT NULL,
  PRIMARY KEY (Status)
);

CREATE TABLE StkTransDetail (
	TransID nvarchar(20) NOT NULL,
	InvtID nvarchar(20) NOT NULL,
	Qty int,
	Amount decimal(18),
	PRIMARY KEY (InvtID,TransID)
);

CREATE TABLE Stock (
	StockID nvarchar(20) NOT NULL,
	StockName nvarchar(50) NOT NULL,
	PRIMARY KEY (StockID)
);

CREATE TABLE StockInspection (
	StockInspID nvarchar(20) NOT NULL,
	StockInspDate date NOT NULL,
	Description nvarchar(200),
	PRIMARY KEY (StockInspID)
);

CREATE TABLE StockTransfer (
	TransID nvarchar(20) NOT NULL,
	TransDate date,
	FromStockID nvarchar(20) NOT NULL,
	ToStockID nvarchar(20) NOT NULL,
	TotalAmt decimal(18),
	Description nvarchar(200),
	PRIMARY KEY (TransID)
);

CREATE TABLE Unit (
	UnitID int NOT NULL,
	UnitName nvarchar(50) NOT NULL,
	PRIMARY KEY (UnitID)
);


CREATE TABLE Vendor (
	VendorID nvarchar(20) NOT NULL,
	VendorName nvarchar(50) NOT NULL,
	Address nvarchar(50),
	Email nvarchar(50),
	Phone nvarchar(20),
	Fax nvarchar(20),
	DueAmt decimal(18),
	Amount decimal(18),
	OverdueAmt decimal(18),
	Status nchar(2) NOT NULL,
	Description nvarchar(200),
	PRIMARY KEY (VendorID)
);


ALTER TABLE Customer
	ADD FOREIGN KEY (Status)
REFERENCES Status (Status);



ALTER TABLE Inventory
	ADD FOREIGN KEY (Status)
REFERENCES Status (Status);

ALTER TABLE Inventory
	ADD FOREIGN KEY (UnitID_L,UnitID_T)
REFERENCES Unit (UnitID,UnitID);



ALTER TABLE Payment
	ADD FOREIGN KEY (CustID)
REFERENCES Customer (CustID);

ALTER TABLE Payment
	ADD FOREIGN KEY (SalesPersonID)
REFERENCES SalesPerson (SalesPersonID);



ALTER TABLE PurchaseOrdDetail
	ADD FOREIGN KEY (InvtID)
REFERENCES Inventory (InvtID);

ALTER TABLE PurchaseOrdDetail
	ADD FOREIGN KEY (OrderNo)
REFERENCES PurchaseOrder (OrderNo);

ALTER TABLE PurchaseOrdDetail
	ADD FOREIGN KEY (StockID)
REFERENCES Stock (StockID);



ALTER TABLE PurchaseOrder
	ADD FOREIGN KEY (OrderType)
REFERENCES OrderType (OrderType);



ALTER TABLE SalesOrder
	ADD FOREIGN KEY (CustID)
REFERENCES Customer (CustID);

ALTER TABLE SalesOrder
	ADD FOREIGN KEY (SalesPersonID)
REFERENCES SalesPerson (SalesPersonID);

ALTER TABLE SalesOrder
	ADD FOREIGN KEY (InvoiceType)
REFERENCES InvoiceType (InvoiceType);



ALTER TABLE SalesPerson
	ADD FOREIGN KEY (Status)
REFERENCES Status (Status);



ALTER TABLE SlsOrderDetail
	ADD FOREIGN KEY (OrderNo)
REFERENCES SalesOrder (OrderNo);



ALTER TABLE StkTransDetail
	ADD FOREIGN KEY (InvtID)
REFERENCES Inventory (InvtID);

ALTER TABLE StkTransDetail
	ADD FOREIGN KEY (TransID)
REFERENCES StockTransfer (TransID);



ALTER TABLE StockTransfer
	ADD FOREIGN KEY (FromStockID,ToStockID)
REFERENCES Stock (StockID,StockID);



ALTER TABLE Vendor
	ADD FOREIGN KEY (Status)
REFERENCES Status (Status);
