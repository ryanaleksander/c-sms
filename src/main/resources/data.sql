INSERT INTO Status(Status, StatusName) VALUES ('AV', 'Active');
INSERT INTO Status(Status, StatusName) VALUES ('UA', 'Inactive');
INSERT INTO Status(Status, StatusName) VALUES ('DE', 'Deleted');

INSERT INTO OrderType(OrderType, TypeName) VALUES ('PO', 'Purchase Order');
INSERT INTO OrderType(OrderType, TypeName) VALUES ('PR', 'Purchase Return');

INSERT INTO MoneyTransaction(TransID, TransAmt, TransDate, Description)
VALUES ('TR001', 2000.0, '2015-8-10', '');

INSERT INTO MoneyTransaction(TransID, TransAmt, TransDate, Description)
VALUES ('TR002', 5000.0, '2015-12-15', '');

INSERT INTO MoneyTransaction(TransID, TransAmt, TransDate, Description)
VALUES ('TR003', 5300.0, '2016-2-11', '');


INSERT INTO InvoiceType(InvoiceType, InvoiceName) VALUES ('NM', 'NM');

INSERT INTO SalesPerson(SalesPersonID, SalesPersonName, Status)
VALUES ('SP1', 'Dư Phát Đạt', 'AV');
INSERT INTO SalesPerson(SalesPersonID, SalesPersonName, Status)
VALUES ('SP2', 'Nguyễn Minh Nhật', 'AV');

INSERT INTO Customer(CustID, CustomerID, Overdue, Status)
VALUES ('Cust1', 'Nguyễn Văn Tài', 7.0 , 'AV');
INSERT INTO Customer(CustID, CustomerID, Overdue, Status)
VALUES ('Cust2', 'Lê Duy Quân', 9.0 , 'AV');

INSERT INTO SalesOrder(OrderNo, OrderDate, InvoiceType, SalesPersonID, CustID, OrderDisc, TaxAmt, TotalAmt, Description)
VALUES ('SO031', '2016-9-10', 'NM', 'SP1', 'Cust1', 5.0, 10.0, 30000.0, '2 món hàng');

INSERT INTO SalesOrder(OrderNo, OrderDate, InvoiceType, SalesPersonID, CustID, OrderDisc, TaxAmt, TotalAmt, Description)
VALUES ('SO032', '2016-9-1', 'NM', 'SP2', 'Cust1', 8.0, 13.0, 76000.0, '4 món hàng');

INSERT INTO SalesOrder(OrderNo, OrderDate, InvoiceType, SalesPersonID, CustID, OrderDisc, TaxAmt, TotalAmt, Description)
VALUES ('SO033', '2016-9-5', 'NM', 'SP1', 'Cust2', 7.0, 6.0, 48000.0, '3 món hàng');

INSERT INTO PurchaseOrder(OrderNo, OrderDate, OrderType, DiscAmt, TaxAmt, TotalAmt)
VALUES ('PO012', '2016-9-10', 'PO', 5.0, 10.0, 30000.0);

INSERT INTO PurchaseOrder(OrderNo, OrderDate, OrderType, DiscAmt, TaxAmt, TotalAmt)
VALUES ('PO013', '2016-9-1', 'PO', 8.0, 13.0, 76000.0);

INSERT INTO PurchaseOrder(OrderNo, OrderDate, OrderType, DiscAmt, TaxAmt, TotalAmt)
VALUES ('PO014', '2016-9-5', 'PO', 7.0, 6.0, 48000.0);

