/*
* RepositoryConfig.java 1.0 2016/8/12
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 12-Aug-2016        AnhLT            Created
* 03-Sep-2016        AnhLT            Add AOP proxy
*/

package vn.fpt.fsoft.csms.configuration;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.Properties;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

/**
 * Provides configuration for database related operations
 * using Hibernate JPA
 *
 * @author Lam Tuan Anh
 * @version 1.0 12-Aug-2016
 */
@Configuration
@ComponentScan(basePackages = "vn.fpt.fsoft.csms")
@PropertySource("classpath:prod.properties")
@EnableTransactionManagement
@EnableAspectJAutoProxy
public class RepositoryConfig {

    @Autowired
    private Environment env;

    /**
     * Set up a database connection for production
     * using properties stored in prod.properties
     * in the resources directory
     *
     * @return a DataSource object
     */
    @Bean(name = "dataSource")
    @Profile("prod")
    public DataSource dataSourceForProd() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(env.getProperty("db.driver"));
        dataSource.setUrl(env.getProperty("db.url"));
        dataSource.setUsername(env.getProperty("db.user"));
        dataSource.setPassword(env.getProperty("db.password"));

        return dataSource;
    }

    /**
     * Set up an embedded H2 database for testing
     * purposes.
     *
     * @return a DataSource object
     */
    @Bean(name = "dataSource")
    @Profile("test")
    public DataSource dataSourceForTest() {
        return new EmbeddedDatabaseBuilder()
                .ignoreFailedDrops(true)
                .generateUniqueName(true)
                .setType(EmbeddedDatabaseType.H2)
                .addScript("schema.sql")
                .addScript("data.sql")
                .build();
    }

    /**
     * Set up a JpaVendorAdapter for production database.
     *
     * @return a JpaVendorAdapter configured for SQL Server
     */
    @Bean(name = "jpaVendorAdapter")
    @Profile("prod")
    public JpaVendorAdapter jpaVendorAdapterForProd() {
        HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
        adapter.setDatabase(Database.SQL_SERVER);
        adapter.setGenerateDdl(true);
        adapter.setShowSql(true);
        return adapter;
    }

    /**
     * Set up a JpaVendorAdapter for testing database.
     *
     * @return a JpaVendorAdapter configured for H2
     */
    @Bean(name = "jpaVendorAdapter")
    @Profile("test")
    public JpaVendorAdapter jpaVendorAdapterForTest() {
        HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
        adapter.setDatabase(Database.H2);
        adapter.setGenerateDdl(true);
        adapter.setShowSql(true);
        return adapter;
    }

    /**
     * Create an EntityManagerFactory for persistence
     * context.
     *
     * @param adapter autowired a JpaVendorAdapter based on
     *                the environment (testing or production)
     * @param dataSource autowired a DataSource based on the
     *                   environment (testing or production)
     * @return an EntityManagerFactory for DAO persistence
     *         context
     */
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean(
            JpaVendorAdapter adapter, DataSource dataSource) {
        Properties prop = new Properties();
        prop.setProperty("hibernate.format_sql", String.valueOf(true));

        LocalContainerEntityManagerFactoryBean bean = new LocalContainerEntityManagerFactoryBean();
        bean.setDataSource(dataSource);
        bean.setJpaVendorAdapter(adapter);
        bean.setPackagesToScan("vn.fpt.fsoft.csms.entities");
        bean.setJpaProperties(prop);

        return bean;
    }

    /**
     * Provides a BeanPostProcessor that translates native
     * exceptions to Spring's DataAccessException.
     *
     * @return a BeanPostProcessor
     */
    @Bean
    public BeanPostProcessor persistenceTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    /**
     * Provides transaction management capabilities to an
     * autowired entity manager factory.
     *
     * @param emf autowired EntityManagerFactory
     * @return a TransactionManager
     */
    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
        return new JpaTransactionManager(emf);
    }
}
