/*
* PurchaseOrdDetail.java 1.0 2016/8/12
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 12-Aug-2016        AnhLT            Created
* 25-Aug-2016        AnhLT            Apply JSON serialization
* 28-Aug-2016        AnhLT            Apply JSON deserialization
*/

package vn.fpt.fsoft.csms.entities;

import com.fasterxml.jackson.annotation.JsonView;
import vn.fpt.fsoft.csms.utils.View;

import javax.persistence.*;

/**
 * PurchaseOrdDetail model
 *
 * @author Lam Tuan Anh
 * @version 1.0 12-Aug-2016
 */
@Entity
public class PurchaseOrdDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(View.Public.class)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "OrderNo", referencedColumnName = "OrderNo")
    private PurchaseOrder purchaseOrder;

    @ManyToOne
    @JsonView(View.Public.class)
    @JoinColumn(name = "InvtID", referencedColumnName = "InvtID")
    private Inventory inventory;

    @JsonView(View.Public.class)
    private Integer qty;

    @JsonView(View.Public.class)
    private Double purchasePrice;

    private Integer qtyProm;

    private Double qtyPromAmt;

    @JsonView(View.Public.class)
    private Double amtProm;

    @JsonView(View.Public.class)
    private Double taxAmt;

    @JsonView(View.Public.class)
    private Double amount;

    @ManyToOne
    @JoinColumn(name = "StockID", referencedColumnName = "StockID")
    private Stock stock;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PurchaseOrder getPurchaseOrder() {
        return purchaseOrder;
    }

    public void setPurchaseOrder(PurchaseOrder purchaseOrder) {
        this.purchaseOrder = purchaseOrder;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Double getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(Double purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public Integer getQtyProm() {
        return qtyProm;
    }

    public void setQtyProm(Integer qtyProm) {
        this.qtyProm = qtyProm;
    }

    public Double getQtyPromAmt() {
        return qtyPromAmt;
    }

    public void setQtyPromAmt(Double qtyPromAmt) {
        this.qtyPromAmt = qtyPromAmt;
    }

    public Double getAmtProm() {
        return amtProm;
    }

    public void setAmtProm(Double amtProm) {
        this.amtProm = amtProm;
    }

    public Double getTaxAmt() {
        return taxAmt;
    }

    public void setTaxAmt(Double taxAmt) {
        this.taxAmt = taxAmt;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        PurchaseOrdDetail that = (PurchaseOrdDetail) obj;

        return id != null ? id.equals(that.id) : that.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "PurchaseOrdDetail{" +
                "id=" + id +
                ", purchaseOrder=" + purchaseOrder +
                ", inventory=" + inventory +
                ", qty=" + qty +
                ", purchasePrice=" + purchasePrice +
                ", amtProm=" + amtProm +
                ", taxAmt=" + taxAmt +
                ", amount=" + amount +
                '}';
    }
}
