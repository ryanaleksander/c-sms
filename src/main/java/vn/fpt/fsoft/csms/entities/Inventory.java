/*
* Inventory.java 1.0 2016/8/12
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 12-Aug-2016        AnhLT            Created
* 25-Aug-2016        AnhLT            Apply JSON serialization
*/

package vn.fpt.fsoft.csms.entities;

import com.fasterxml.jackson.annotation.JsonView;
import vn.fpt.fsoft.csms.utils.View;

import javax.persistence.*;

/**
 * Inventory model
 *
 * @author Lam Tuan Anh
 * @version 1.0 12-Aug-2016
 */
@Entity
public class Inventory {

    @Id
    @JsonView(View.Public.class)
    private String invtId;

    @JsonView(View.Public.class)
    private String invtName;

    @JsonView(View.Public.class)
    private String className;

    @ManyToOne
    @JoinColumn(name = "UnitID_T", referencedColumnName = "UnitID")
    private Unit unitT;

    @ManyToOne
    @JsonView(View.Public.class)
    @JoinColumn(name = "UnitID_L", referencedColumnName = "UnitID")
    private Unit unitL;

    private Integer unitRate;

    private Double salesPriceT;

    @JsonView(View.Public.class)
    private Double salesPriceL;

    @JsonView(View.Public.class)
    private Integer qtyStock;

    @JsonView(View.Public.class)
    private Double slsTax;

    @ManyToOne
    @JoinColumn(name = "Status", referencedColumnName = "Status")
    private Status status;

    public String getInvtId() {
        return invtId;
    }

    public void setInvtId(String invtId) {
        this.invtId = invtId;
    }

    public String getInvtName() {
        return invtName;
    }

    public void setInvtName(String invtName) {
        this.invtName = invtName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Unit getUnitT() {
        return unitT;
    }

    public void setUnitT(Unit unitT) {
        this.unitT = unitT;
    }

    public Unit getUnitL() {
        return unitL;
    }

    public void setUnitL(Unit unitL) {
        this.unitL = unitL;
    }

    public Integer getUnitRate() {
        return unitRate;
    }

    public void setUnitRate(Integer unitRate) {
        this.unitRate = unitRate;
    }

    public Double getSalesPriceT() {
        return salesPriceT;
    }

    public void setSalesPriceT(Double salesPriceT) {
        this.salesPriceT = salesPriceT;
    }

    public Double getSalesPriceL() {
        return salesPriceL;
    }

    public void setSalesPriceL(Double salesPriceL) {
        this.salesPriceL = salesPriceL;
    }

    public Integer getQtyStock() {
        return qtyStock;
    }

    public void setQtyStock(Integer qtyStock) {
        this.qtyStock = qtyStock;
    }

    public Double getSlsTax() {
        return slsTax;
    }

    public void setSlsTax(Double slsTax) {
        this.slsTax = slsTax;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Inventory inventory = (Inventory) obj;

        return invtId.equals(inventory.invtId);

    }

    @Override
    public int hashCode() {
        return invtId.hashCode();
    }
}
