/*
* SlsOrderDetail.java 1.0 2016/8/18
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 18-Aug-2016        QuanLD            Created
*/

package vn.fpt.fsoft.csms.entities;

import com.fasterxml.jackson.annotation.JsonView;

import vn.fpt.fsoft.csms.utils.View;

import javax.persistence.*;

/**
 * SalesOrderDetail model
 *
 * @author Le Duy Quan
 * @version 1.0 18-Aug-2016
 */
@Entity
public class SlsOrderDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(View.Public.class)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "OrderNo", referencedColumnName = "OrderNo")
    private SalesOrder salesOrder;

    @ManyToOne
    @JsonView(View.Public.class)
    @JoinColumn(name = "InvtID", referencedColumnName = "InvtID")
    private Inventory inventory;

    @JsonView(View.Public.class)
    private int qty;

    @JsonView(View.Public.class)
    private Double salesPrice;

    @JsonView(View.Public.class)
    private Double discount;

    @JsonView(View.Public.class)
    private Double taxAmt;

    @JsonView(View.Public.class)
    private Double amount;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SalesOrder getSalesOrder() {
        return salesOrder;
    }

    public void setSalesOrder(SalesOrder salesOrder) {
        this.salesOrder = salesOrder;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public Double getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(Double salesPrice) {
        this.salesPrice = salesPrice;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Double getTaxAmt() {
        return taxAmt;
    }

    public void setTaxAmt(Double taxAmt) {
        this.taxAmt = taxAmt;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        SlsOrderDetail that = (SlsOrderDetail) obj;

        return id != null ? id.equals(that.id) : that.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
