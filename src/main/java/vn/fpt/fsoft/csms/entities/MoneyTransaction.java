/*
* MoneyTransaction.java 1.0 2016/8/12
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* ------------------------------------------------------------
* 12-Aug-2016        AnhLT            Created
* 23-Aug-2016        AnhLT            Apply JSON serialization
*/

package vn.fpt.fsoft.csms.entities;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.format.annotation.DateTimeFormat;
import vn.fpt.fsoft.csms.utils.View;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;

/**
 * MoneyTransaction model
 *
 * @author Lam Tuan Anh
 * @version 1.0 12-Aug-2016
 */
@Entity
public class MoneyTransaction {

    @Id
    @JsonView(View.Public.class)
    private String transId;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @JsonView(View.Public.class)
    private Date transDate;

    @Min(value = 0)
    @JsonView(View.Public.class)
    private Double transAmt;

    @JsonView(View.Public.class)
    private String description;

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public Date getTransDate() {
        return transDate;
    }

    public void setTransDate(Date transDate) {
        this.transDate = transDate;
    }

    public Double getTransAmt() {
        return transAmt;
    }

    public void setTransAmt(Double transAmt) {
        this.transAmt = transAmt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        MoneyTransaction that = (MoneyTransaction) obj;

        return transId.equals(that.transId);

    }

    @Override
    public int hashCode() {
        return transId.hashCode();
    }

    @Override
    public String toString() {
        return "MoneyTransaction{" +
                "transId='" + transId + '\'' +
                ", transDate=" + transDate +
                ", transAmt=" + transAmt +
                ", description='" + description + '\'' +
                '}';
    }
}
