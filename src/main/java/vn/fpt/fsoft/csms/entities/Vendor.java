/*
* Vendor.java 1.0 2016/8/25
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 25-Aug-2016        LongNH           Created
*/

package vn.fpt.fsoft.csms.entities;

import com.fasterxml.jackson.annotation.JsonView;
import vn.fpt.fsoft.csms.utils.View;

import javax.persistence.*;

/**
 * Vendor
 *
 * @author Nguyen Hoang Long
 * @version 1.0 25-Aug-2016
 */
@Entity
public class Vendor {

    @Id
    @JsonView(View.Public.class)
    private String vendorId;

    @JsonView(View.Public.class)
    private String vendorName;

    private String address;

    private String email;

    private String phone;

    private String fax;

    private Double dueAmt;

    private Double amount;

    private Double overdueAmt;

    @ManyToOne
    @JoinColumn(name = "Status", referencedColumnName = "Status")
    private Status status;

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public Double getDueAmt() {
        return dueAmt;
    }

    public void setDueAmt(Double dueAmt) {
        this.dueAmt = dueAmt;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getOverdueAmt() {
        return overdueAmt;
    }

    public void setOverdueAmt(Double overdueAmt) {
        this.overdueAmt = overdueAmt;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Vendor vendor = (Vendor) obj;

        return vendorId.equals(vendor.vendorId);

    }

    @Override
    public int hashCode() {
        return vendorId.hashCode();
    }
}
