/*
* Customer.java 1.0 2016/8/25
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 25-Aug-2016        LongNH           Created
*/

package vn.fpt.fsoft.csms.entities;

import com.fasterxml.jackson.annotation.JsonView;
import vn.fpt.fsoft.csms.utils.View;

import javax.persistence.*;

/**
 * Customer
 *
 * @author Nguyen Hoang Long
 * @version 1.0 25-Aug-2016
 */
@Entity
public class Customer {

    @Id
    @JsonView(View.Public.class)
    private String custId;

    @JsonView(View.Public.class)
    private String customerName;

    private String address;

    private String phone;

    private String fax;
    
    private String email;

    private Integer overdue;

    private Double amount;

    private Double overdueAmt;
    
    private Double dueAmt;

    @ManyToOne
    @JoinColumn(name = "Status", referencedColumnName = "Status")
    private Status status;

    public String getCustId() {
        return custId;
    }

    public void setcustId(String custId) {
        this.custId = custId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public Integer getOverdue() {
        return overdue;
    }

    public void setOverdue(Integer overdue) {
        this.overdue = overdue;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getOverdueAmt() {
        return overdueAmt;
    }

    public void setOverdueAmt(Double overdueAmt) {
        this.overdueAmt = overdueAmt;
    }
    
    public Double getDueAmt() {
        return dueAmt;
    }

    public void setDueAmt(Double dueAmt) {
        this.dueAmt = dueAmt;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Customer customer = (Customer) obj;

        return custId.equals(customer.custId);

    }

    @Override
    public int hashCode() {
        return custId.hashCode();
    }
}
