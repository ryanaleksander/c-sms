/*
* SalesPerson.java 1.0 2016/8/25
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 25-Aug-2016        QuanLD           Created
*/

package vn.fpt.fsoft.csms.entities;

import com.fasterxml.jackson.annotation.JsonView;
import vn.fpt.fsoft.csms.utils.View;

import javax.persistence.*;

/**
 * SalesPerson
 *
 * @author Le Duy Quan
 * @version 1.0 25-Aug-2016
 */
@Entity
public class SalesPerson {

    @Id
    @JsonView(View.Public.class)
    private String salesPersonId;

    @JsonView(View.Public.class)
    private String salesPersonName;

    private String address;

    private Integer stockQuota;

    private String description;
    
    @ManyToOne
    @JoinColumn(name = "Status", referencedColumnName = "Status")
    private Status status;

    public String getSalesPersonId() {
        return salesPersonId;
    }

    public void setSalesPersonId(String salesPersonId) {
        this.salesPersonId = salesPersonId;
    }

    public String getSalesPersonName() {
        return salesPersonName;
    }

    public void setSalesPersonName(String salesPersonName) {
        this.salesPersonName = salesPersonName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    
    public Integer getStockQuota() {
    	return stockQuota;
    }
    
    public void setStockQuota(Integer stockQuota) {
        this.stockQuota = stockQuota;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        SalesPerson salesPerson = (SalesPerson) obj;

        return salesPersonId.equals(salesPerson.salesPersonId);

    }

    @Override
    public int hashCode() {
        return salesPersonId.hashCode();
    }
}
