/*
* InvoiceType.java 1.0 2016/8/18
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 18-Aug-2016        QuanLD            Created
*/

package vn.fpt.fsoft.csms.entities;

import com.fasterxml.jackson.annotation.JsonView;
import vn.fpt.fsoft.csms.utils.View;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * InvoiceType model
 *
 * @author Le Duy Quan
 * @version 1.0 18-Aug-2016
 */
@Entity
public class InvoiceType {

    @Id
    @JsonView(View.Public.class)
    private String invoiceType;

    private String invoiceName;

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getInvoiceName() {
        return invoiceName;
    }

    public void setInvoiceName(String invoiceName) {
        this.invoiceName = invoiceName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        InvoiceType invoiceType1 = (InvoiceType) obj;

        return invoiceType.equals(invoiceType1.invoiceType);

    }

    @Override
    public int hashCode() {
        return invoiceType.hashCode();
    }
}
