/*
* PurchaseOrder.java 1.0 2016/8/12
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 12-Aug-2016        AnhLT            Created
* 25-Aug-2016        AnhLT            Apply JSON serialization
* 28-Aug-2016        AnhLT            Apply JSON deserialization
*/

package vn.fpt.fsoft.csms.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.deser.std.DateDeserializers;
import vn.fpt.fsoft.csms.utils.View;

import java.util.Date;
import java.util.Set;
import javax.persistence.*;

/**
 * PurchaseOrder model
 *
 * @author Lam Tuan Anh
 * @version 1.0 12-Aug-2016
 */
@Entity
public class PurchaseOrder {

    @Id
    @JsonView(View.Public.class)
    private String orderNo;

    @JsonDeserialize(using = DateDeserializers.DateDeserializer.class)
    @JsonFormat(pattern = "dd-MM-yyyy")
    @Temporal(TemporalType.DATE)
    @JsonView(View.Public.class)
    private Date orderDate;

    @ManyToOne
    @JsonView(View.Public.class)
    @JoinColumn(name = "OrderType", referencedColumnName = "OrderType")
    private OrderType orderType;

    @Temporal(TemporalType.DATE)
    private Date overdueDate;

    @JsonView(View.Public.class)
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "purchaseOrder")
    private Set<PurchaseOrdDetail> purchaseOrdDetails;

    @JsonView(View.Public.class)
    private Double discAmt;

    private Double comAmt;

    @JsonView(View.Public.class)
    private Double taxAmt;

    @JsonView(View.Public.class)
    private Double totalAmt;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public OrderType getOrderType() {
        return orderType;
    }

    public void setOrderType(OrderType orderType) {
        this.orderType = orderType;
    }

    public Date getOverdueDate() {
        return overdueDate;
    }

    public void setOverdueDate(Date overdueDate) {
        this.overdueDate = overdueDate;
    }

    public Double getDiscAmt() {
        return discAmt;
    }

    public void setDiscAmt(Double discAmt) {
        this.discAmt = discAmt;
    }

    public Double getComAmt() {
        return comAmt;
    }

    public void setComAmt(Double comAmt) {
        this.comAmt = comAmt;
    }

    public Double getTaxAmt() {
        return taxAmt;
    }

    public void setTaxAmt(Double taxAmt) {
        this.taxAmt = taxAmt;
    }

    public Double getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(Double totalAmt) {
        this.totalAmt = totalAmt;
    }

    public Set<PurchaseOrdDetail> getPurchaseOrdDetails() {
        return purchaseOrdDetails;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        PurchaseOrder that = (PurchaseOrder) obj;

        return orderNo.equals(that.orderNo);
    }

    @Override
    public int hashCode() {
        return orderNo.hashCode();
    }

    @Override
    public String toString() {
        return "PurchaseOrder{" +
                "orderNo='" + orderNo + '\'' +
                ", orderDate=" + orderDate +
                ", orderType=" + orderType +
                ", discAmt=" + discAmt +
                ", taxAmt=" + taxAmt +
                '}';
    }
}
