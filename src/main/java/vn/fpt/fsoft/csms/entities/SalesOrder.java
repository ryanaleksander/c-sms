/*
* SalesOrder.java 1.0 2016/8/18
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 18-Aug-2016        QuanLD            Created
*/

package vn.fpt.fsoft.csms.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.deser.std.DateDeserializers;
import vn.fpt.fsoft.csms.utils.View;

import java.util.Date;
import java.util.Set;
import javax.persistence.*;

/**
 * SalesOrder model
 *
 * @author Le Duy Quan
 * @version 1.0 18-Aug-2016
 */
@Entity
public class SalesOrder {

    @Id
    @JsonView(View.Public.class)
    private String orderNo;

    @JsonDeserialize(using = DateDeserializers.DateDeserializer.class)
    @JsonFormat(pattern = "dd-MM-yyyy")
    @Temporal(TemporalType.DATE)
    @JsonView(View.Public.class)
    private Date orderDate;

    @ManyToOne
    @JsonView(View.Public.class)
    @JoinColumn(name = "InvoiceType", referencedColumnName = "InvoiceType")
    private InvoiceType invoiceType;
    
    @Temporal(TemporalType.DATE)
    private Date overdueDate;
    
    @ManyToOne
    @JsonView(View.Public.class)
    @JoinColumn(name = "CustID", referencedColumnName = "CustID")
    private Customer custId;
    
    @ManyToOne
    @JsonView(View.Public.class)
    @JoinColumn(name = "SalesPersonID", referencedColumnName = "SalesPersonID")
    private SalesPerson salesPersonId;
    
    @ManyToOne
    @JsonView(View.Public.class)
    @JoinColumn(name = "VendorID", referencedColumnName = "VendorID")
    private Vendor vendorId;

    @JsonView(View.Public.class)
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "salesOrder")
    private Set<SlsOrderDetail> slsOrderDetails;

    public Set<SlsOrderDetail> getSlsOrderDetail() {
        return slsOrderDetails;
    }
    
    @JsonView(View.Public.class)
    private Double orderDisc;

    @JsonView(View.Public.class)
    private Double taxAmt;

    @JsonView(View.Public.class)
    private Double totalAmt;

    private Double payment;

    @JsonView(View.Public.class)
    private String description;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public InvoiceType getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(InvoiceType invoiceType) {
        this.invoiceType = invoiceType;
    }

    public Customer getCustomerId() {
        return custId;
    }

    public void setCustomerId(Customer custId) {
        this.custId = custId;
    }
    
    public SalesPerson getSalesPersonId() {
        return salesPersonId;
    }

    public void setSalesPersonId(SalesPerson salesPersonId) {
        this.salesPersonId = salesPersonId;
    }
    
    public Vendor getVendorId() {
        return vendorId;
    }

    public void setVendorId(Vendor vendorId) {
        this.vendorId = vendorId;
    }

    public Date getOverdueDate() {
        return overdueDate;
    }

    public void setOverdueDate(Date overdueDate) {
        this.overdueDate = overdueDate;
    }

    public Double getOrderDisc() {
        return orderDisc;
    }

    public void setOrderDisc(Double orderDisc) {
        this.orderDisc = orderDisc;
    }

    public Double getTaxAmt() {
        return taxAmt;
    }

    public void setTaxAmt(Double taxAmt) {
        this.taxAmt = taxAmt;
    }

    public Double getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(Double totalAmt) {
        this.totalAmt = totalAmt;
    }

    public Double getPayment() {
        return payment;
    }

    public void setPayment(Double payment) {
        this.payment = payment;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        SalesOrder that = (SalesOrder) obj;

        return orderNo.equals(that.orderNo);

    }

    @Override
    public int hashCode() {
        return orderNo.hashCode();
    }
}
