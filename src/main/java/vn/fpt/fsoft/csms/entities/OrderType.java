/*
* OrderType.java 1.0 2016/8/12
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 12-Aug-2016        AnhLT            Created
* 25-Aug-2016        AnhLT            Apply JSON serialization
*/

package vn.fpt.fsoft.csms.entities;

import com.fasterxml.jackson.annotation.JsonView;
import vn.fpt.fsoft.csms.utils.View;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * OrderType model
 *
 * @author Lam Tuan Anh
 * @version 1.0 12-Aug-2016
 */
@Entity
public class OrderType {

    @Id
    @JsonView(View.Public.class)
    private String orderType;

    private String typeName;

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderName() {
        return typeName;
    }

    public void setOrderName(String orderName) {
        this.typeName = orderName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        OrderType orderType1 = (OrderType) obj;

        return orderType.equals(orderType1.orderType);

    }

    @Override
    public int hashCode() {
        return orderType.hashCode();
    }
}
