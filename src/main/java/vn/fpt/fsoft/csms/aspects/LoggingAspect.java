/*
* LogginAspect.java 1.0 2016/8/12
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 03-Sep-2016        AnhLT            Created
*/

package vn.fpt.fsoft.csms.aspects;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Provides logging for database-related operations
 *
 * @author Lam Tuan Anh
 * @version 1.0 3-Sep-2016
 */
@Aspect
@Component
public class LoggingAspect {

    private Logger logger = LoggerFactory.getLogger(LoggingAspect.class);

    /**
     * Logging for create methods
     *
     * @param obj entity to be created and added to the database
     */
    @After("execution(void vn.fpt.fsoft.csms.dao.impl.*.create*(*)) && args(obj)")
    public void createEntity(Object obj) {
        logger.info("Create new " + obj.getClass().toString() + ":\n" + obj.toString());
    }

    /**
     * Logging for delete methods
     *
     * @param obj entity to be removed from the database
     */
    @After("execution(void vn.fpt.fsoft.csms.dao.impl.*.delete*(*)) && args(obj)")
    public void deleteEntity(Object obj) {
        logger.info("Delete " + obj.getClass().toString() + ":\n" + obj.toString());
    }
}
