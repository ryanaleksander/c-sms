/*
* SalesOrderDaoImpl.java 1.0 2016/8/12
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 18-Aug-2016        QuanLD            Created
*/

package vn.fpt.fsoft.csms.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.fpt.fsoft.csms.dao.SalesOrderDao;
import vn.fpt.fsoft.csms.entities.SalesOrder;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
@Transactional
public class SalesOrderDaoImpl implements SalesOrderDao {

    @PersistenceContext
    private EntityManager entityManager;

    public SalesOrder getSalesOrder(String orderNo) {
        return entityManager.find(SalesOrder.class, orderNo);
    }

    public List<SalesOrder> getSalesOrderList() {
        return entityManager.createQuery("select a from SalesOrder a", SalesOrder.class)
                .getResultList();
    }

    public void createSalesOrder(SalesOrder salesOrder) {
        entityManager.persist(salesOrder);
    }

    public void deleteSalesOrder(String orderNo) {
        entityManager.remove(getSalesOrder(orderNo));
    }
}
