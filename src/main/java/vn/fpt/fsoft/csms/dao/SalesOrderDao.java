/*
* SalesOrderDao.java 1.0 2016/8/18
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 18-Aug-2016        QuanLD            Created
*/

package vn.fpt.fsoft.csms.dao;

import vn.fpt.fsoft.csms.entities.SalesOrder;

import java.util.List;

/**
 * Provides basic DAO operations
 *
 * @author Le Duy Quan
 * @version 1.0 18-Aug-2016
 */
public interface SalesOrderDao {

    SalesOrder getSalesOrder(String orderNo);

    List<SalesOrder> getSalesOrderList();

    void createSalesOrder(SalesOrder salesOrder);

    void deleteSalesOrder(String orderNo);
}
