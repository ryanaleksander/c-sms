/*
* ImplementationDaoImpl.java 1.0 2016/8/12
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 25-Aug-2016        LongNH           Created
*/

package vn.fpt.fsoft.csms.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.fpt.fsoft.csms.dao.VendorDao;
import vn.fpt.fsoft.csms.entities.Vendor;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Implementation of the VendorDao interface.
 *
 * @author Nguyen Hoang Long
 * @version 1.0 25-Aug-2016
 */
@Repository
@Transactional
public class VendorDaoImpl implements VendorDao {

    @PersistenceContext
    private EntityManager entityManager;

    public Vendor getVendor(String vendorId) {
        return entityManager.find(Vendor.class, vendorId);
    }

    public List<Vendor> getVendorList() {
        return entityManager.createQuery("select a from Vendor a", Vendor.class)
                .getResultList();
    }

    public void createVendor(Vendor vendor) {
        entityManager.persist(vendor);
    }

    public void deleteVendor(String vendorId) {
        entityManager.remove(getVendor(vendorId));
    }

}
