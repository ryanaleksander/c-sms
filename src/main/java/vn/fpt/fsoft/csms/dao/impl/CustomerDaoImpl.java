/*
* ImplementationDaoImpl.java 1.0 2016/8/12
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 25-Aug-2016        LongNH           Created
*/

package vn.fpt.fsoft.csms.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.fpt.fsoft.csms.dao.CustomerDao;
import vn.fpt.fsoft.csms.entities.Customer;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Implementation of the CustomerDao interface.
 *
 * @author Nguyen Hoang Long
 * @version 1.0 25-Aug-2016
 */
@Repository
@Transactional
public class CustomerDaoImpl implements CustomerDao {

    @PersistenceContext
    private EntityManager entityManager;

    public Customer getCustomer(String custId) {
        return entityManager.find(Customer.class, custId);
    }

    public List<Customer> getCustomerList() {
        return entityManager.createQuery("select a from Customer a", Customer.class)
                .getResultList();
    }

    public void createCustomer(Customer customer) {
        entityManager.persist(customer);
    }

    public void deleteCustomer(String custId) {
        entityManager.remove(getCustomer(custId));
    }

}
