/*
* PurchaseOrderDaoImpl.java 1.0 2016/8/12
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 12-Aug-2016        AnhLT            Created
*/

package vn.fpt.fsoft.csms.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.fpt.fsoft.csms.dao.PurchaseOrderDao;
import vn.fpt.fsoft.csms.entities.PurchaseOrder;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Implementation of the PurchaseOrderDao interface.
 *
 * @author Lam Tuan Anh
 * @version 1.0 12-Aug-2016
 */
@Repository
@Transactional
public class PurchaseOrderDaoImpl implements PurchaseOrderDao {

    @PersistenceContext
    private EntityManager entityManager;

    public PurchaseOrder getPurchaseOrder(String orderNo) {
        return entityManager.find(PurchaseOrder.class, orderNo);
    }

    public List<PurchaseOrder> getPurchaseOrderList() {
        return entityManager
                .createQuery(
                        "select a from PurchaseOrder a where a.orderType = 'PO'",
                        PurchaseOrder.class)
                .getResultList();
    }

    public List<PurchaseOrder> getPurchaseReturnList() {
        return entityManager
                .createQuery(
                        "select a from PurchaseOrder a where a.orderType = 'PR'",
                        PurchaseOrder.class)
                .getResultList();
    }

    public void createPurchaseOrder(PurchaseOrder purchaseOrder) {
        entityManager.persist(purchaseOrder);
    }

    public void deletePurchaseOrder(String orderNo) {
        entityManager.remove(getPurchaseOrder(orderNo));
    }
}
