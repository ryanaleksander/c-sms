/*
* InvoiceTypeDao.java 1.0 2016/8/18
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 18-Aug-2016        QuanLD            Created
*/

package vn.fpt.fsoft.csms.dao;


import vn.fpt.fsoft.csms.entities.InvoiceType;

import java.util.List;

/**
 * Provides basic DAO operations
 *
 * @author Le Duy Quan
 * @version 1.0 18-Aug-2016
 */
public interface InvoiceTypeDao {
    
    InvoiceType getInvoiceType(String invoiceType);
    
    List<InvoiceType> getInvoiceTypeList();
    
    void createInvoiceType(InvoiceType invoiceType);
    
    void deleteInvoiceType(String invoiceType);
}
