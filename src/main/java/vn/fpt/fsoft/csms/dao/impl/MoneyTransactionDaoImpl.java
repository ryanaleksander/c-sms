/*
* MoneyTransactionDaoImpl.java 1.0 2016/8/12
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 12-Aug-2016        AnhLT            Created
*/

package vn.fpt.fsoft.csms.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.fpt.fsoft.csms.dao.MoneyTransactionDao;
import vn.fpt.fsoft.csms.entities.MoneyTransaction;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Implementation of the MoneyTransactionDao interface.
 *
 * @author Lam Tuan Anh
 * @version 1.0 12-Aug-2016
 */
@Repository
@Transactional
public class MoneyTransactionDaoImpl implements MoneyTransactionDao {

    @PersistenceContext
    private EntityManager entityManager;

    public MoneyTransaction getMoneyTransaction(String transactionId) {
        return entityManager.find(MoneyTransaction.class, transactionId);
    }

    public List<MoneyTransaction> getMoneyTransactionList() {
        return entityManager
                .createQuery("select a from MoneyTransaction a", MoneyTransaction.class)
                .getResultList();
    }

    public void createMoneyTransaction(MoneyTransaction moneyTransaction) {
        entityManager.persist(moneyTransaction);
    }

    public void deleteMoneyTransaction(String transactionId) {
        entityManager.remove(getMoneyTransaction(transactionId));
    }
}
