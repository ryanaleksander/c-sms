/*
* PurchaseOrdDetailDaoImpl.java 1.0 2016/8/12
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 12-Aug-2016        AnhLT            Created
*/

package vn.fpt.fsoft.csms.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.fpt.fsoft.csms.dao.PurchaseOrdDetailDao;
import vn.fpt.fsoft.csms.entities.PurchaseOrdDetail;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Implementation of the PurchaseOrdDetailDao interface.
 *
 * @author Lam Tuan Anh
 * @version 1.0 12-Aug-2016
 */
@Repository
@Transactional
public class PurchaseOrdDetailDaoImpl implements PurchaseOrdDetailDao {

    @PersistenceContext
    private EntityManager entityManager;

    public PurchaseOrdDetail getPurchaseOrdDetail(Integer id) {
        return entityManager.find(PurchaseOrdDetail.class, id);
    }

    public List<PurchaseOrdDetail> getPurchaseOrdDetailList(String orderNo) {
        return entityManager
                .createQuery("select a from PurchaseOrdDetail a", PurchaseOrdDetail.class)
                .getResultList();
    }

    public void createPurchaseOrdDetail(PurchaseOrdDetail purchaseOrdDetail) {
        entityManager.persist(purchaseOrdDetail);
    }

    public void deletePurchaseOrdDetail(Integer id) {
        entityManager.remove(getPurchaseOrdDetail(id));
    }
}
