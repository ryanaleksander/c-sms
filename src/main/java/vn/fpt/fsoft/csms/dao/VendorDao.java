/*
* VendorDao.java 1.0 2016/8/12
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 25-Aug-2016        LongNH            Created
*/

package vn.fpt.fsoft.csms.dao;

import vn.fpt.fsoft.csms.entities.Vendor;

import java.util.List;

/**
 * Provides basic DAO operations
 *
 * @author Nguyen Hoang Long
 * @version 1.0 25-Aug-2016
 */
public interface VendorDao {
    Vendor getVendor(String vendorId);
    
    List<Vendor> getVendorList();
    
    void createVendor(Vendor vendor);
    
    void deleteVendor(String vendorId);
}
