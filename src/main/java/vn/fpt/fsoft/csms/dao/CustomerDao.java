/*
* VendorDao.java 1.0 2016/8/12
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 25-Aug-2016        LongNH            Created
*/

package vn.fpt.fsoft.csms.dao;

import vn.fpt.fsoft.csms.entities.Customer;

import java.util.List;

/**
 * Provides basic DAO operations
 *
 * @author Nguyen Hoang Long
 * @version 1.0 25-Aug-2016
 */
public interface CustomerDao {

    Customer getCustomer(String custId);

    List<Customer> getCustomerList();

    void createCustomer(Customer customer);

    void deleteCustomer(String custId);
}
