/*
* ImplementationDaoImpl.java 1.0 2016/8/12
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 25-Aug-2016        QuanLD           Created
*/

package vn.fpt.fsoft.csms.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.fpt.fsoft.csms.dao.SalesPersonDao;
import vn.fpt.fsoft.csms.entities.SalesPerson;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Implementation of the SalesPersonDao interface.
 *
 * @author Le Duy Quan
 * @version 1.0 25-Aug-2016
 */
@Repository
@Transactional
public class SalesPersonDaoImpl implements SalesPersonDao {

    @PersistenceContext
    private EntityManager entityManager;

    public SalesPerson getSalesPerson(String salesPersonId) {
        return entityManager.find(SalesPerson.class, salesPersonId);
    }

    public List<SalesPerson> getSalesPersonList() {
        return entityManager.createQuery("select a from SalesPerson a", SalesPerson.class)
                .getResultList();
    }

    public void createSalesPerson(SalesPerson salesPerson) {
        entityManager.persist(salesPerson);
    }

    public void deleteSalesPerson(String salesPerson) {
        entityManager.remove(getSalesPerson(salesPerson));
    }
}
