/*
* SlsOrderDetailDaoImpl.java 1.0 2016/8/12
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 18-Aug-2016        QuanLD            Created
*/

package vn.fpt.fsoft.csms.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.fpt.fsoft.csms.dao.SlsOrderDetailDao;
import vn.fpt.fsoft.csms.entities.SlsOrderDetail;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Implementation of the SlsOrderDetailDao interface.
 *
 * @author Le Duy Quan
 * @version 1.0 18-Aug-2016
 */
@Repository
@Transactional
public class SlsOrderDetailDaoImpl implements SlsOrderDetailDao {

    @PersistenceContext
    private EntityManager entityManager;

    public SlsOrderDetail getSlsOrderDetail(Integer id) {
        return entityManager.find(SlsOrderDetail.class, id);
    }

    public List<SlsOrderDetail> getSlsOrderDetailList(String orderNo) {
        return entityManager.createQuery("select a from SlsOrderDetail a", SlsOrderDetail.class)
                .getResultList();
    }

    public void createSlsOrderDetail(SlsOrderDetail slsOrderDetail) {
        entityManager.persist(slsOrderDetail);
    }

    public void deleteSlsOrderDetail(Integer id) {
        entityManager.remove(getSlsOrderDetail(id));
    }

}
