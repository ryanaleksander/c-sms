/*
* StockDaoImpl.java 1.0 2016/8/12
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 12-Aug-2016        AnhLT            Created
*/

package vn.fpt.fsoft.csms.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.fpt.fsoft.csms.dao.StockDao;
import vn.fpt.fsoft.csms.entities.Stock;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Implementation of the StatusDao interface.
 *
 * @author Lam Tuan Anh
 * @version 1.0 12-Aug-2016
 */
@Repository
@Transactional
public class StockDaoImpl implements StockDao {

    @PersistenceContext
    private EntityManager entityManager;

    public Stock getStock(String stockId) {
        return entityManager.find(Stock.class, stockId);
    }

    public List<Stock> getStockList() {
        return entityManager.createQuery("select a from Stock a", Stock.class)
                .getResultList();
    }

    public void createStock(Stock stock) {
        entityManager.persist(stock);
    }

    public void deleteStock(String stockId) {
        entityManager.remove(getStock(stockId));
    }
}
