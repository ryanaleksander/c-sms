/*
* InvoiceTypeDaoImpl.java 1.0 2016/8/12
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 18-Aug-2016        QuanLD            Created
*/

package vn.fpt.fsoft.csms.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.fpt.fsoft.csms.dao.InvoiceTypeDao;
import vn.fpt.fsoft.csms.entities.InvoiceType;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Implementation of the InvoiceTypeDao interface.
 *
 * @author Le Duy Quan
 * @version 1.0 18-Aug-2016
 */
@Repository
@Transactional
public class InvoiceTypeDaoImpl implements InvoiceTypeDao {

    @PersistenceContext
    private EntityManager entityManager;

    public InvoiceType getInvoiceType(String invoiceType) {
        return entityManager.find(InvoiceType.class, invoiceType);
    }

    public List<InvoiceType> getInvoiceTypeList() {
        return entityManager.createQuery("select a from InvoiceType a", InvoiceType.class)
                .getResultList();
    }

    public void createInvoiceType(InvoiceType invoiceType) {
        entityManager.persist(invoiceType);
    }

    public void deleteInvoiceType(String invoiceType) {
        entityManager.remove(getInvoiceType(invoiceType));
    }
}
