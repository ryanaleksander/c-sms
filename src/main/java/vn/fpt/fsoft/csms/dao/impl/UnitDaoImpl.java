/*
* UnitDaoImpl.java 1.0 2016/8/12
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 12-Aug-2016        AnhLT            Created
*/

package vn.fpt.fsoft.csms.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.fpt.fsoft.csms.dao.UnitDao;
import vn.fpt.fsoft.csms.entities.Unit;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Implementation of the StatusDao interface.
 *
 * @author Lam Tuan Anh
 * @version 1.0 12-Aug-2016
 */
@Repository
@Transactional
public class UnitDaoImpl implements UnitDao {

    @PersistenceContext
    private EntityManager entityManager;

    public Unit getUnit(Integer unitId) {
        return entityManager.find(Unit.class, unitId);
    }

    public List<Unit> getUnitList() {
        return entityManager.createQuery("select a from Unit a", Unit.class)
                .getResultList();
    }

    public void createUnit(Unit unit) {
        entityManager.persist(unit);
    }

    public void deleteUnit(Integer unitId) {
        entityManager.remove(getUnit(unitId));
    }
}
