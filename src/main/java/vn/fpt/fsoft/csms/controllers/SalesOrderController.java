package vn.fpt.fsoft.csms.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import vn.fpt.fsoft.csms.dao.CustomerDao;
import vn.fpt.fsoft.csms.dao.InventoryDao;
import vn.fpt.fsoft.csms.dao.InvoiceTypeDao;
import vn.fpt.fsoft.csms.dao.SalesOrderDao;
import vn.fpt.fsoft.csms.dao.SalesPersonDao;
import vn.fpt.fsoft.csms.entities.Customer;
import vn.fpt.fsoft.csms.entities.InvoiceType;
import vn.fpt.fsoft.csms.entities.SalesOrder;
import vn.fpt.fsoft.csms.entities.SalesPerson;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Controller for sales order services
 *
 * @author LongNH
 * @version 1.0 1-Sep-2016
 */
@Controller
public class SalesOrderController {
    @Autowired
    private SalesOrderDao salesOrderDao;

    @Autowired
    private InventoryDao inventoryDao;

    @Autowired
    private InvoiceTypeDao invoiceTypeDao;

    @Autowired
    private SalesPersonDao salesPersonDao;
    
    @Autowired
    private CustomerDao customerDao;
    
    /**
     * Handling show the list of SalesOrder
     */
    @RequestMapping("/sales_order")
    public String salesOrder(Model model) {
        model.addAttribute("salesOrder", salesOrderDao.getSalesOrderList());    
        model.addAttribute("sizet", salesOrderDao.getSalesOrderList().size());
        return "sales_order";
    }
    
    /**
     * Handling add new SalesOrder
     */
    @RequestMapping("/create_sales_order")
    public String createSalesOrder(Model model) {
        model.addAttribute("salesOrder", new SalesOrder());
        model.addAttribute("salesOrders", salesOrderDao.getSalesOrderList());
        model.addAttribute("inventories", inventoryDao.getInventoryList());
        model.addAttribute("salesPerson", salesPersonDao.getSalesPersonList());
        model.addAttribute("customer", customerDao.getCustomerList());
        return "create_sales_order";
    }

    /**
     * Handling create_sales_order form submission
     */
    @RequestMapping(value = "/create_sales_order/{id}/{id1}", method = RequestMethod.POST)
    public String createSalesOrderPost(
            @ModelAttribute("salesOrder") SalesOrder salesOrder,
            @PathVariable("id") String salesPersonId,
            @PathVariable("id1") String customerId) {
        SalesPerson salesPerson = salesPersonDao.getSalesPerson(salesPersonId);
        salesOrder.setSalesPersonId(salesPerson);
        Customer customer = customerDao.getCustomer(customerId);
        salesOrder.setCustomerId(customer);
        InvoiceType invoiceType = invoiceTypeDao.getInvoiceType("NP");
        salesOrder.setInvoiceType(invoiceType);
        salesOrderDao.createSalesOrder(salesOrder);
        return "redirect:/sales_order";
    }

    /**
     * Format submitted date data
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }
}
