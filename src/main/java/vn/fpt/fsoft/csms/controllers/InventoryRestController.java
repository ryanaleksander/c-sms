/*
* InventoryRestController.java 1.0 2016/8/15
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* -----------------------------------------------------------------
* 28-Aug-2016        AnhLT            Created
*/

package vn.fpt.fsoft.csms.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.fpt.fsoft.csms.dao.InventoryDao;
import vn.fpt.fsoft.csms.entities.Inventory;
import vn.fpt.fsoft.csms.utils.View;

import java.util.List;

/**
 * Controller for transaction REST services
 *
 * @author Lam Tuan Anh
 * @version 1.0 28-Aug-2016
 */
@RestController
@RequestMapping("/inventories/api")
public class InventoryRestController {

    @Autowired
    private InventoryDao inventoryDao;

    @JsonView(View.Public.class)
    @RequestMapping("/inventoryList")
    public List<Inventory> inventory() {
        return inventoryDao.getInventoryList();
    }
}
