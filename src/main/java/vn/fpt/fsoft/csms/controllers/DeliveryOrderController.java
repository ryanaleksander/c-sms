/*
* DeliveryOrderController.java 1.0 2016/8/24
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* -----------------------------------------------------------------
* 24-Aug-2016        QuanLD            Created
* 27-Aug-2016        QuanLD            Updated
*/

package vn.fpt.fsoft.csms.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import vn.fpt.fsoft.csms.dao.InventoryDao;
import vn.fpt.fsoft.csms.dao.InvoiceTypeDao;
import vn.fpt.fsoft.csms.dao.SalesOrderDao;
import vn.fpt.fsoft.csms.dao.SalesPersonDao;
import vn.fpt.fsoft.csms.dao.VendorDao;
import vn.fpt.fsoft.csms.entities.InvoiceType;
import vn.fpt.fsoft.csms.entities.SalesOrder;
import vn.fpt.fsoft.csms.entities.SalesPerson;
import vn.fpt.fsoft.csms.entities.Vendor;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Controller for delivery order related requests
 *
 * @author Le Duy Quan
 * @version 1.2 27-Aug-2016
 */
@Controller
public class DeliveryOrderController {

    @Autowired
    private SalesOrderDao salesOrderDao;

    @Autowired
    private InventoryDao inventoryDao;

    @Autowired
    private InvoiceTypeDao invoiceTypeDao;

    @Autowired
    private SalesPersonDao salesPersonDao;

    @Autowired
    private VendorDao vendorDao;

    /**
     * Handling show the list of DeliveryOrders
     */
    @RequestMapping("/delivery_orders")
    public String deliveryOrders(Model model) {
        model.addAttribute("deliveryOrders", salesOrderDao.getSalesOrderList());
        model.addAttribute("sizet", salesOrderDao.getSalesOrderList().size());
        return "delivery_orders";
    }

    /**
     * Handling add new DeliveryOrder
     */
    @RequestMapping("/create_delivery_order")
    public String createDeliveryOrder(Model model) {
        model.addAttribute("salesOrder", new SalesOrder());
        model.addAttribute("salesOrders", salesOrderDao.getSalesOrderList());
        model.addAttribute("inventories", inventoryDao.getInventoryList());
        model.addAttribute("salesPerson", salesPersonDao.getSalesPersonList());
        model.addAttribute("vendor", vendorDao.getVendorList());
        return "create_delivery_order";
    }

    /**
     * Handling create_delivery_order form submission
     */
    @RequestMapping(value = "/create_delivery_order/{id}/{id1}", method = RequestMethod.POST)
    public String createDeliveryOrderPost(
            @ModelAttribute("salesOrder") SalesOrder salesOrder,
            @PathVariable("id") String salesPersonId,
            @PathVariable("id1") String vendorId) {
        SalesPerson salesPerson = salesPersonDao.getSalesPerson(salesPersonId);
        salesOrder.setSalesPersonId(salesPerson);
        Vendor vendor = vendorDao.getVendor(vendorId);
        salesOrder.setVendorId(vendor);
        InvoiceType invoiceType = invoiceTypeDao.getInvoiceType("ND");
        salesOrder.setInvoiceType(invoiceType);
        salesOrderDao.createSalesOrder(salesOrder);
        return "redirect:/delivery_orders";
    }

    /**
     * Format submitted date data
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }
}
