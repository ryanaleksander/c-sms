/*
* PurchaseOrderRestController.java 1.0 2016/8/15
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* -----------------------------------------------------------------
* 28-Aug-2016        AnhLT            Created
*/

package vn.fpt.fsoft.csms.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import vn.fpt.fsoft.csms.dao.PurchaseOrdDetailDao;
import vn.fpt.fsoft.csms.dao.PurchaseOrderDao;
import vn.fpt.fsoft.csms.entities.PurchaseOrdDetail;
import vn.fpt.fsoft.csms.entities.PurchaseOrder;
import vn.fpt.fsoft.csms.utils.View;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Controller for transaction related requests
 *
 * @author Lam Tuan Anh
 * @version 1.0 28-Aug-2016
 */
@RestController
@RequestMapping("/purchaseOrders/api")
public class PurchaseOrderRestController {

    @Autowired
    private PurchaseOrderDao purchaseOrderDao;

    @Autowired
    private PurchaseOrdDetailDao purchaseOrdDetailDao;

    @JsonView(View.Public.class)
    @RequestMapping("/purchaseOrderList")
    public List<PurchaseOrder> getPurchaseOrderList() {
        return purchaseOrderDao.getPurchaseOrderList();
    }

    @JsonView(View.Public.class)
    @RequestMapping("/purchaseReturnList")
    public List<PurchaseOrder> getPurchaseReturnList() {
        return purchaseOrderDao.getPurchaseReturnList();
    }

    /**
     * Handling post requests to create new PurchaseOrdDetails
     */
    @RequestMapping(value = "/details/{orderNo}", method = RequestMethod.POST)
    public
    @ResponseBody
    String createDetails(
            @PathVariable("orderNo") String orderNo,
            @RequestBody PurchaseOrdDetail[] details) {
        PurchaseOrder purchaseOrder;

        // New PurchaseOrder may not yet exist
        do {
            purchaseOrder = purchaseOrderDao.getPurchaseOrder(orderNo);
        } while (purchaseOrder == null);

        for (PurchaseOrdDetail detail : details) {
            detail.setPurchaseOrder(purchaseOrder);
            purchaseOrdDetailDao.createPurchaseOrdDetail(detail);
        }
        return "success";
    }

    /**
     * Format submitted date data
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }
}
