/*
* LoginController.java 1.0 2016/8/15
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 15-Aug-2016        AnhLT            Created
*/

package vn.fpt.fsoft.csms.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Controller for login related requests
 *
 * @author Lam Tuan Anh
 * @version 1.0 15-Aug-2016
 */
@Controller
public class LoginController {

    /**
     * Handling login requests
     */
    @RequestMapping("/login")
    public String login(@RequestParam(value = "error", required = false) String error,
                        @RequestParam(value = "logout", required = false) String logout,
                        Model model) {

        if (error != null) {
            model.addAttribute("error", "Tên đăng nhập hoặc mật khẩu không hợp lệ");
        }

        if (logout != null) {
            model.addAttribute("msg", "Đã đăng xuất thành công");
        }

        return "login";
    }

    /**
     * Handling logout requests
     */
    @RequestMapping("/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout";
    }

}
