/*
* CustomerRestController.java 1.0 2016/8/27
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* -----------------------------------------------------------------
* 27-Aug-2016        QuanLD            Created
*/

package vn.fpt.fsoft.csms.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.fpt.fsoft.csms.dao.CustomerDao;
import vn.fpt.fsoft.csms.entities.Customer;
import vn.fpt.fsoft.csms.utils.View;

import java.util.List;


/**
 * Controller for Customer REST services
 *
 * @author Le Duy Quan
 * @version 1.0 27-Aug-2016
 */
@RestController
@RequestMapping("/customer/api")
public class CustomerRestController {
    
    @Autowired
    private CustomerDao customerDao;
    
    /**
     * List of Customer 
     */
    @JsonView(View.Public.class)
    @RequestMapping("/customerList")
    public List<Customer> getCustomerList() {
        return customerDao.getCustomerList();
    }
}
