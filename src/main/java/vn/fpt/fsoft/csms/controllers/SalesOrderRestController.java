/*
* SalesOrdersRestController.java 1.0 2016/8/24
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* -----------------------------------------------------------------
* 24-Aug-2016        QuanLD            Created
* 26-Aug-2016        QuanLD            Updated
* 27-Aug-2016        QuanLD            Class separation
*/

package vn.fpt.fsoft.csms.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import vn.fpt.fsoft.csms.dao.SalesOrderDao;
import vn.fpt.fsoft.csms.dao.SlsOrderDetailDao;
import vn.fpt.fsoft.csms.entities.SalesOrder;
import vn.fpt.fsoft.csms.entities.SlsOrderDetail;
import vn.fpt.fsoft.csms.utils.View;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Controller for Sales Orders REST services
 *
 * @author Le Duy Quan
 * @version 1.3 27-Aug-2016
 */
@RestController
@RequestMapping("/salesOrders/api")
public class SalesOrderRestController {

    @Autowired
    private SalesOrderDao salesOrderDao;

    @Autowired
    private SlsOrderDetailDao slsOrderDetailDao;


    /**
     * List of ReturnedGoods
     */
    @JsonView(View.Public.class)
    @RequestMapping("/salesOrderList")
    public List<SalesOrder> getReturnGoodsList() {
        return salesOrderDao.getSalesOrderList();
    }

    /**
     * Handling post requests to create new ReturnedGood detail
     */
    @RequestMapping(value = "/details/{orderNo}", method = RequestMethod.POST)
    public
    @ResponseBody
    String getDetails(
            @PathVariable("orderNo") String orderNo,
            @RequestBody SlsOrderDetail[] details) {
        SalesOrder salesOrder;

        // New ReturnedGood may not yet exist
        do {
            salesOrder = salesOrderDao.getSalesOrder(orderNo);
        }
        while (salesOrder == null);
        for (SlsOrderDetail detail : details) {
            detail.setSalesOrder(salesOrder);
            slsOrderDetailDao.createSlsOrderDetail(detail);
        }
        return "success";
    }

    /**
     * Format submitted date data
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }
}
