/*
* ReturnGoodsController.java 1.0 2016/8/24
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* -----------------------------------------------------------------
* 24-Aug-2016        QuanLD            Created
* 27-Aug-2016        QuanLD            Updated
*/
package vn.fpt.fsoft.csms.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import vn.fpt.fsoft.csms.dao.CustomerDao;
import vn.fpt.fsoft.csms.dao.InventoryDao;
import vn.fpt.fsoft.csms.dao.InvoiceTypeDao;
import vn.fpt.fsoft.csms.dao.SalesOrderDao;
import vn.fpt.fsoft.csms.dao.SalesPersonDao;
import vn.fpt.fsoft.csms.entities.Customer;
import vn.fpt.fsoft.csms.entities.InvoiceType;
import vn.fpt.fsoft.csms.entities.SalesOrder;
import vn.fpt.fsoft.csms.entities.SalesPerson;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Controller for returned goods related requests
 *
 * @author Le Duy Quan
 * @version 1.2 27-Aug-2016
 */
@Controller
public class ReturnedGoodsController {
    @Autowired
    private SalesOrderDao salesOrderDao;
	
    @Autowired
    private InventoryDao inventoryDao;

    @Autowired
    private InvoiceTypeDao invoiceTypeDao;

    @Autowired
    private SalesPersonDao salesPersonDao;
    
    @Autowired
    private CustomerDao customerDao;
    
    /**
     * Handling show the list of ReturnedGood 
     */
    @RequestMapping("/returned_goods")
    public String returnedGoods(Model model) {
        model.addAttribute("returnedGoods", salesOrderDao.getSalesOrderList());    
        model.addAttribute("sizet", salesOrderDao.getSalesOrderList().size());
        return "returned_goods";
    }
    
    /**
     * Handling add new ReturnedGoods 
     */
    @RequestMapping("/create_returned_good")
    public String createReturnedGood(Model model) {
        model.addAttribute("salesOrder", new SalesOrder());
        model.addAttribute("salesOrders", salesOrderDao.getSalesOrderList());
        model.addAttribute("inventories", inventoryDao.getInventoryList());
        model.addAttribute("salesPerson", salesPersonDao.getSalesPersonList());
        model.addAttribute("customer", customerDao.getCustomerList());
        return "create_returned_good";
    }

    /**
     * Handling create_returned_good form submission
     */
    @RequestMapping(value = "/create_returned_good/{id}/{id1}", method = RequestMethod.POST)
    public String createReturnedGoodPost(
            @ModelAttribute("salesOrder") SalesOrder salesOrder,
            @PathVariable("id") String salesPersonId,
            @PathVariable("id1") String customerId) {
        SalesPerson salesPerson = salesPersonDao.getSalesPerson(salesPersonId);
        salesOrder.setSalesPersonId(salesPerson);
        Customer customer = customerDao.getCustomer(customerId);
        salesOrder.setCustomerId(customer);
        InvoiceType invoiceType = invoiceTypeDao.getInvoiceType("NM");
        salesOrder.setInvoiceType(invoiceType);
        salesOrderDao.createSalesOrder(salesOrder);
        return "redirect:/returned_goods";
    }

    /**
     * Format submitted date data
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }
}
