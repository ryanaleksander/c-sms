/*
* TransactionController.java 1.0 2016/8/15
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* -------------------------------------------------------------------------------
* 15-Aug-2016        AnhLT            Created
* 23-Aug-2016        AnhLT            Implement post request handler
* 29-Aug-2016        AnhLT            Add transaction list to GET request handler
*/

package vn.fpt.fsoft.csms.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import vn.fpt.fsoft.csms.dao.MoneyTransactionDao;
import vn.fpt.fsoft.csms.entities.MoneyTransaction;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Controller for transaction related requests
 *
 * @author Lam Tuan Anh
 * @version 1.0 15-Aug-2016
 */
@Controller
public class TransactionController {

    @Autowired
    private MoneyTransactionDao moneyTransactionDao;

    @RequestMapping("/transaction")
    public String transaction(Model model) {
        List<MoneyTransaction> transactions = moneyTransactionDao.getMoneyTransactionList();
        model.addAttribute("transactions", transactions);

        return "transaction";
    }

    @RequestMapping("/create_transaction")
    public String createTransaction(Model model) {
        model.addAttribute("moneyTransaction", new MoneyTransaction());
        model.addAttribute("transactions", moneyTransactionDao.getMoneyTransactionList());
        return "create_transaction";
    }

    /**
     * Handling create_transaction form submission
     */
    @RequestMapping(value = "/create_transaction", method = RequestMethod.POST)
    public String createTransactionPost(
            @ModelAttribute("moneyTransaction") MoneyTransaction transaction
    ) {
 
        moneyTransactionDao.createMoneyTransaction(transaction);

        return "redirect:/transaction";
    }

    /**
     * Format submitted date data
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }
}
