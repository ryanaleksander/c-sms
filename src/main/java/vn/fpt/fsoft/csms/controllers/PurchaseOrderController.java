/*
* PurchaseOrderController.java 1.0 2016/8/15
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* -----------------------------------------------------------------
* 24-Aug-2016        AnhLT            Created
*/

package vn.fpt.fsoft.csms.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import vn.fpt.fsoft.csms.dao.InventoryDao;
import vn.fpt.fsoft.csms.dao.OrderTypeDao;
import vn.fpt.fsoft.csms.dao.PurchaseOrderDao;
import vn.fpt.fsoft.csms.entities.OrderType;
import vn.fpt.fsoft.csms.entities.PurchaseOrder;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Controller for purchase order related requests
 *
 * @author Lam Tuan Anh
 * @version 1.0 24-Aug-2016
 */
@Controller
public class PurchaseOrderController {

    @Autowired
    private PurchaseOrderDao purchaseOrderDao;

    @Autowired
    private InventoryDao inventoryDao;

    @Autowired
    private OrderTypeDao orderTypeDao;

    @RequestMapping("/purchase_orders")
    public String purchaseOrders(Model model) {
        model.addAttribute("purchaseOrders", purchaseOrderDao.getPurchaseOrderList());

        return "purchase_orders";
    }

    @RequestMapping("/create_purchase_order")
    public String createPurchaseOrder(Model model) {
        model.addAttribute("purchaseOrder", new PurchaseOrder());
        model.addAttribute("purchaseOrders", purchaseOrderDao.getPurchaseOrderList());
        model.addAttribute("inventories", inventoryDao.getInventoryList());

        return "create_purchase_order";
    }

    /**
     * Handling create_purchase _rder form submission
     */
    @RequestMapping(value = "/create_purchase_order", method = RequestMethod.POST)
    public String createPurchaseOrderPost(
            @ModelAttribute("purchaseOrder") PurchaseOrder purchaseOrder) {

        OrderType orderType = orderTypeDao.getOrderType("PO");
        purchaseOrder.setOrderType(orderType);
        purchaseOrderDao.createPurchaseOrder(purchaseOrder);

        return "redirect:/purchase_orders";
    }

    /**
     * Format submitted date data
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }
}
