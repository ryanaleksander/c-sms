/*
* View.java 1.0 2016/8/15
*
* Copyright (c) 2016 Fpt Corporation.
* FPT Building, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District,
* Hanoi, Vietnam.
* All rights reserved.
*
* This software is the confidential and proprietary information of Fpt
* Corporation. ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Fpt.*
*
* Modification Logs:
* DATE               AUTHOR           DESCRIPTION
* --------------------------------------------------------
* 23-Aug-2016        AnhLT            Created
*/

package vn.fpt.fsoft.csms.utils;

/**
 * Dummy class used for JsonView
 *
 * @author Lam Tuan Anh
 * @version 1.0 23-Aug-2016
 */
public class View {
    public static class Public {}
}
