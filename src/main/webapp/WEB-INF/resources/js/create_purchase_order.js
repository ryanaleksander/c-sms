$(document).ready(function() {
	// Validate messages
	validator.message.date = 'Ngày không hợp lệ';
	validator.message.empty = 'Thông tin bắt buộc';
	validator.message.invalid = 'Dữ liệu không hợp lệ';

	// CSRF params
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");

	// Elements
	var orderDate = $('#order-date');
	var orderNo = $('#order-no');
	var taxAmt = $('#tax-amt');
	var discAmt = $('#disc-amt');
	var totalAmt = $('#total-amt');
	var btnSubmit = $('#submit-btn');
	var datatable = $('#datatable').DataTable({
		"language": {
			"emptyTable": "Không có dữ liệu"
		}
	});
	var ordersModal = $('#order-modal');
	var ordersTable = $('#modal-datatable').DataTable({
		"language": {
			"emptyTable": "Không có dữ liệu"
		}
	});

	// Other variables
	var orders = [];
	var inventories = [];
	var details = [];
	var totalTax = 0;
	var totalDisc = 0;
	var grandTotal = 0;
	var original = 0;

	// Ajax requests to get data
	$.getJSON(ctx + "/inventories/api/inventoryList", function(data) {
		inventories = data;
	});

	$.getJSON(ctx + "/purchaseOrders/api/purchaseOrderList", function(data){
		orders = orders.concat(data);
	});

	$.getJSON(ctx + "/purchaseOrders/api/purchaseReturnList", function(data) {
		orders = orders.concat(data);
	});

	orderDate.on('hide.daterangepicker', function() {
		orderDate.val(orderDate.val()).change();
	});

	// Add inventory popover initialization
	var popover = $('#add-inventory').popover({
		html : true,
		title: function() {
			return $("#popover-head").html();
		},
		content: function() {
			return $("#popover-content").html();
		}
	});

	// F2 event
	orderNo.on("keyup", function(e) {
		if (e.keyCode === 113) {
			e.preventDefault();
			ordersModal.modal('show');
		}
	});

	// Select order
	$('body').on("click", ".select-btn", function() {
		ordersModal.modal('hide');
		orderNo.val($(this).parent().parent().children(':first').text()).change();
	});

	// Add inventory event
	$('body').on('click', '#inventory-submit', function(e) {
		var qty = $('#qty').val();
		var prom = $('#prom').val();
		var id = $('#invt-id').val();

		// Validate inputs
		if (qty.match(/^((\d+(\.\d*)?)|(\.\d+))$/)
			&& prom.match(/^((\d+(\.\d*)?)|(\.\d+))$/)) {

			var invt = inventories.filter(function(i) { return i.invtId == id})[0];

			// Calculate values for purchase order
			var total = Number(qty) * invt.salesPriceL;
			original += total;
			totalDisc += total * Number(prom)/100;
			total -= total * Number(prom)/100;
			totalTax += total * invt.slsTax/100;
			total += total * invt.slsTax/100;
			grandTotal += total;

			// Add to datatable
			datatable.row.add([
				id,
				invt.invtName,
				qty,
				invt.salesPriceL,
				invt.unitL.unitName,
				prom,
				total
			]).draw(false);

			id = Number(id);
			qty = Number(qty);
			prom = Number(prom);

			// Add data to send to server
			details.push({
				inventory: invt,
				qty: qty,
				purchasePrice: invt.salesPriceL,
				amtProm: prom,
				taxAmt: invt.slsTax,
				amount: total
			});

			taxAmt.val(totalTax / original * 100);
			discAmt.val(totalDisc / original * 100);
			totalAmt.val(grandTotal);
		} else {
			// Display error when inputs are invalid
			new PNotify({
				title: "Lỗi",
				text: 'Dữ liệu không hợp lệ',
				type: "error",
				hide: false,
				styling: "bootstrap3"
			})
		}
	});

	orderDate.on("keydown", function(event) {
		event.preventDefault();
	});

	// Date picker initialization
	orderDate.daterangepicker({
		format: 'DD-MM-YYYY',
		singleDatePicker: true,
		calender_style: "picker_4"
	});

	var notify = null;

	// Check if orderNo exists
	orderNo.on("keyup change", function() {
		// Clear the form
		datatable.clear().draw();
		taxAmt.val(0);
		discAmt.val(0);
		totalAmt.val(0);
		totalTax = 0;
		totalDisc = 0;
		grandTotal = 0;
		original = 0;
		details = [];

		for (var i = 0; i < orders.length; i++) {
			if (orders[i].orderNo == this.value) {
				// If orderNo already exists
				if (orderDate.data('daterangepicker')) {
					orderDate.data('daterangepicker').remove();
				}
				// Format the receive date
				orderDate.val(moment(orders[i].orderDate, 'YYYY-MM-DD').format('DD-MM-YYYY').toString()).change();
				taxAmt.val(orders[i].taxAmt);
				discAmt.val(orders[i].discAmt);
				totalAmt.val(orders[i].totalAmt);
				btnSubmit.attr('disabled', true);
				popover.attr('disabled', true);
				orders[i].purchaseOrdDetails.forEach(function(detail) {
					datatable.row.add([
						detail.inventory.invtId,
						detail.inventory.invtName,
						detail.qty,
						detail.purchasePrice,
						detail.inventory.unitL.unitName,
						detail.amtProm,
						detail.amount
					]).draw(false);
				});
				if (notify === null) {
					notify = new PNotify({
						title: "Chú ý",
						text: "Số hóa đơn đã tồn tại",
						type: "info",
						hide: true,
						delay: 3000,
						styling: "bootstrap3"
					});
				}
				return;
			}
		}

		// If orderNo doesn't exist
		orderDate.daterangepicker({
			format: 'DD-MM-YYYY',
			singleDatePicker: true,
			calender_style: "picker_4"
		});
		btnSubmit.attr('disabled', false);
		popover.attr('disabled', false);
		notify = null;
	});

	// Form validation
	$('form')
		.on('blur', 'input[required], input.optional, select.required', validator.checkField)
		.on('change', 'select.required, input[required]', validator.checkField)
		.on('keypress', 'input[required][pattern]', validator.keypress);

	$('.multi.required').on('keyup blur', 'input', function() {
		validator.checkField.apply($(this).siblings().last()[0]);
	});

	// Submit event
	$('#order-form').submit(function(e) {
		e.preventDefault();
		var canSubmit = true;

		if (!validator.checkAll($(this))) {
			canSubmit = false;
		}

		// Send ajax to add purchaseOrdDetails
		$.ajax({
			url: ctx + '/purchaseOrders/api/details/' + orderNo.val(),
			method: 'post',
			contentType: 'json',
			data: JSON.stringify(details),
			beforeSend: function(xhr) {
				xhr.setRequestHeader(header, token);
				xhr.setRequestHeader("Accept", "application/json");
				xhr.setRequestHeader("Content-Type", "application/json");
			}
		});

		if (canSubmit)
			this.submit();

		return false;
	});
});