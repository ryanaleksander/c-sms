$(document).ready(function() {
	validator.message.date = 'Ngày không hợp lệ';
	validator.message.empty = 'Thông tin bắt buộc';

	var transDate = $('#trans-date');
	var transId = $('#trans-id');
	var transAmt = $('#trans-amt');
	var description = $('#description');
	var submit = $('#submit-btn');
	var transModal = $('#transaction-modal');
	var modalDatatable = $('#modal-datatable').DataTable({
		"language": {
			"emptyTable": "Không có dữ liệu"
		}
	});
	var transactions = [];

	$.getJSON(ctx + "/transactions/api/transactionList", function(data){
		transactions = data;
	});

	transDate.on("keydown", function(event) {
		event.preventDefault();
	});

	transDate.daterangepicker({
		format: 'DD-MM-YYYY',
		singleDatePicker: true,
		calender_style: "picker_4"
	});

	transDate.on('hide.daterangepicker', function() {
		transDate.val(transDate.val()).change();
	});

	var notify = null;

	transId.on("keyup change", function() {
		for (var i = 0; i < transactions.length; i++) {
			if (transactions[i].transId == this.value) {
				if (transDate.data('daterangpicker')) {
					transDate.data('daterangepicker').remove();
				}
				transDate.val(moment(transactions[i].transDate, 'YYYY-MM-DD').format('DD-MM-YYYY').toString()).change();
				transAmt.attr("readonly", true);
				transAmt.val(transactions[i].transAmt);
				description.attr("readonly", true);
				description.val(transactions[i].description);
				submit.attr("disabled", true);

				if (notify === null) {
					notify = new PNotify({
						title: "Chú ý",
						text: "Mã chuyển tiền đã tồn tại",
						type: "info",
						hide: true,
						delay: 3000,
						styling: "bootstrap3"
					});
				}
				return;
			}
		}
		transDate.daterangepicker({
			format: 'DD-MM-YYYY',
			singleDatePicker: true,
			calender_style: "picker_4"
		});
		transAmt.attr("readonly", false);
		description.attr("readonly", false);
		submit.attr('disabled', false)
		notify = null;
	});

	$('form')
		.on('blur', 'input[required], input.optional, select.required', validator.checkField)
		.on('change', 'select.required, input[required]', validator.checkField)
		.on('keypress', 'input[required][pattern]', validator.keypress);

	$('.multi.required').on('keyup blur', 'input', function() {
		validator.checkField.apply($(this).siblings().last()[0]);
	});

	$('form').submit(function(e) {
		e.preventDefault();
		var submit = true;

		if (!validator.checkAll($(this))) {
			submit = false;
		}

		if (submit)
			this.submit();

		return false;
	});

	transId.on("keyup", function(e) {
		if (e.keyCode === 113) {
			e.preventDefault();
			transModal.modal('show');
		}
	});

	$('body').on("click", ".select-btn", function() {
		transModal.modal('hide');
		transId.val($(this).parent().parent().children(':first').text()).change();
	});
});