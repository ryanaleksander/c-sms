$(document).ready(function() {
	// Validate messages
	validator.message.date = 'Ngày không hợp lệ';
	validator.message.empty = 'Thông tin bắt buộc';
	validator.message.invalid = 'Dữ liệu không hợp lệ';

	// CSRF params
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");

	// Elements
	var orderDate = $('#order-date');
	var orderNo = $('#order-no');
	var salespersonId = $('#sales-personId');
	var salespersonName = $('#sales-personName');
	var customerId = $('#customer-id');
	var customerName = $('#customer-name');
	var description = $('#description');
	var taxAmt = $('#tax-amt');
	var orderDisc = $('#order-disc');
	var totalAmt = $('#total-amt');
	var btnSubmit = $('#submit-btn');
	var datatable = $('#datatable').DataTable({
		"language": {
			"emptyTable": "Không có dữ liệu"
		}
	});
	var ordersModal = $('#order-modal');
	var salesPersonModal = $('#salesPerson-modal');
	var customerModal = $('#customer-modal');
	var ordersTable = $('#modal-datatable').DataTable({
		"language": {
			"emptyTable": "Không có dữ liệu"
		}
	});

	// Other variables
	var orders = [];
	var salesPs = [];
	var customers = [];
	var inventories = [];
	var details = [];
	var totalTax = 0;
	var totalDisc = 0;
	var grandTotal = 0;
	var original = 0;

	// Ajax requests to get data
	$.getJSON(ctx + "/inventories/api/inventoryList", function(data) {
		inventories = data;
	});

	$.getJSON(ctx + "/salesOrders/api/salesOrderList", function(data){
		orders = data;
	});
	
	$.getJSON(ctx + "/salesPerson/api/salesPersonList", function(data){
		salesPs = data;
	});
	
	$.getJSON(ctx + "/customer/api/customerList", function(data){
		customers = data;
	});

	orderDate.on('hide.daterangepicker', function() {
		orderDate.val(orderDate.val()).change();
	});
	
	// Add inventory popover initialization
	var popover = $('#add-inventory').popover({
		html : true,
		title: function() {
			return $("#popover-head").html();
		},
		content: function() {
			return $("#popover-content").html();
		}
	});

	// F2 event
	orderNo.on("keyup", function(e) {
		if (e.keyCode === 113) {
			e.preventDefault();
			ordersModal.modal('show');
		}
	});
	
	salespersonId.on("keyup", function(e) {
		if (e.keyCode === 113) {
			e.preventDefault();
			salesPersonModal.modal('show');
		}
	});
	
	customerId.on("keyup", function(e) {
		if (e.keyCode === 113) {
			e.preventDefault();
			customerModal.modal('show');
		}
	});

	// Select order
	$('body').on("click", "#selectOrder", function() {
		ordersModal.modal('hide');
		orderNo.val($(this).parent().parent().children(':first').text()).change();
	});
	
	$('body').on("click", "#selectPerson", function() {
		salesPersonModal.modal('hide');
		salespersonId.val($(this).parent().parent().children(':first').text()).change();
	});
	
	$('body').on("click", "#selectCustomer", function() {
		customerModal.modal('hide');
		customerId.val($(this).parent().parent().children(':first').text()).change();
	});

	// Add inventory event
	$('body').on('click', '#inventory-submit', function(e) {
		var qty = $('#qty').val();
		var prom = $('#prom').val();
		var id = $('#invt-id').val();

		// Validate inputs
		if (qty.match(/^((\d+(\.\d*)?)|(\.\d+))$/)
			&& prom.match(/^((\d+(\.\d*)?)|(\.\d+))$/)) {

			var invt = inventories.filter(function(i) { return i.invtId == id})[0];		
			
			// Calculate values for returned good
			var total = Number(qty) * invt.salesPriceL;
			original += total;
			totalDisc += total * Number(prom)/100;
			total -= total * Number(prom)/100;
			totalTax += total * invt.slsTax/100;
			total += total * invt.slsTax/100;
			grandTotal += total;

			// Add to datatable
			datatable.row.add([
				id,
				invt.invtName,
				qty,
				invt.salesPriceL,
				invt.unitL.unitName,
				prom,
				invt.slsTax,
				total
			]).draw(false);

			id = Number(id);
			qty = Number(qty);
			prom = Number(prom);

			// Add data to send to server
			details.push({
				inventory: invt,
				qty: qty,
				salesPrice: invt.salesPriceL,
				discount: prom,
				taxAmt: invt.slsTax,
				amount: total
			});

			taxAmt.val(totalTax / original * 100);
			orderDisc.val(totalDisc / original * 100);
			totalAmt.val(grandTotal);
		} else {
			// Display error when inputs are invalid
			new PNotify({
				title: "Lỗi",
				text: 'Dữ liệu không hợp lệ',
				type: "error",
				hide: false,
				styling: "bootstrap3"
			})
		}
	});

	orderDate.on("keydown", function(event) {
		event.preventDefault();
	});

	// Date picker initialization
	orderDate.daterangepicker({
		format: 'DD-MM-YYYY',
		singleDatePicker: true,
		calender_style: "picker_4"
	});

	var confi1 = true;
	var confi2 = true;
	var notify = null;

	salespersonName.prop('readonly', true);
	customerName.prop('readonly', true);

	// Check if orderNo exists
	orderNo.on("keyup change", function() {
		// Clear the form
		datatable.clear().draw();
		taxAmt.val(0);
		orderDisc.val(0);
		totalAmt.val(0);
		salespersonId.val(null);
		salespersonName.val(null);
		customerId.val(null);
		customerName.val(null);
		description.val(null);
		totalTax = 0;
		totalDisc = 0;
		grandTotal = 0;
		original = 0;
		details = [];

		for (var i = 0; i < orders.length; i++) {
			if (orders[i].orderNo == this.value ) {
				if (orders[i].invoiceType.invoiceType != 'NM'){
					if (notify === null) {
						notify = new PNotify({
							title: "Chú ý",
							text: "Số giao dịch không cho phép",
							type: "error",
							hide: true,
							delay: 3000,
							styling: "bootstrap3"
						});
					}
					btnSubmit.attr('disabled', true);
					popover.attr('disabled', true);
					salespersonId.prop('readonly', true);
					customerId.prop('readonly', true);
					description.prop('readonly', true);
					return;
				}
				// If orderNo already exists
				if (orderDate.data('daterangepicker')) {
					orderDate.data('daterangepicker').remove();
				}
				// Format the receive date
				orderDate.val(moment(orders[i].orderDate, 'YYYY-MM-DD').format('DD-MM-YYYY').toString()).change();;
				salespersonId.val(orders[i].salesPersonId.salesPersonId).toString();
				salespersonName.val(orders[i].salesPersonId.salesPersonName).toString();
				customerId.val(orders[i].custId.custID).toString();
				customerName.val(orders[i].custId.customerName).toString();
				description.val(orders[i].description).toString();
				taxAmt.val(orders[i].taxAmt);
				orderDisc.val(orders[i].orderDisc);	
				totalAmt.val(orders[i].totalAmt);
				btnSubmit.attr('disabled', true);
				popover.attr('disabled', true);
				salespersonId.prop('readonly', true);
				customerId.prop('readonly', true);
				description.prop('readonly', true);
				orders[i].slsOrderDetails.forEach(function(detail) {
					datatable.row.add([
						detail.inventory.invtId,
						detail.inventory.invtName,
						detail.qty,
						detail.salesPrice,
						detail.inventory.unitL.unitName,
						detail.discount,
						detail.taxAmt,
						detail.amount
					]).draw(false);
				});
				if (notify === null) {
					notify = new PNotify({
						title: "Chú ý",
						text: "Số giao dịch đã tồn tại",
						type: "info",
						hide: true,
						delay: 3000,
						styling: "bootstrap3"
					});
				}
				return;
			}
		}
		// If orderNo doesn't exist
		orderDate.daterangepicker({
			format: 'DD-MM-YYYY',
			singleDatePicker: true,
			calender_style: "picker_4"
		});
		btnSubmit.attr('disabled', false);
		popover.attr('disabled', false);
		salespersonId.prop('readonly', false);
		customerId.prop('readonly', false);
		description.prop('readonly', false);
		notify = null;
	});
	
	salespersonId.on("keyup change", function() {
		salespersonName.val(null);
		for (var i = 0; i < salesPs.length; i++) {
			if (salesPs[i].salesPersonId == this.value) {
				confi1 = true;
				salespersonName.val(salesPs[i].salesPersonName).toString();	
//					new PNotify({
//						title: "Thông báo",
//						text: "Mã nhân viên hợp lệ",
//						type: "info",
//						hide: true,
//						delay: 2000,
//						styling: "bootstrap3"
//					});
				return;	
			}
			else{
				confi1 = false;		
			}
		}		
	});
	
	customerId.on("keyup change", function() {
		customerName.val(null);
		for (var i = 0; i < customers.length; i++) {
			if (customers[i].custID == this.value) {
				confi2 = true;
				customerName.val(customers[i].customerName).toString();	
//					 new PNotify({
//						title: "Thông báo",
//						text: "Mã khách hàng hợp lệ",
//						type: "info",
//						hide: true,
//						delay: 2000,
//						styling: "bootstrap3"
//					});
				return;	
			}
			else{
				confi2 = false;		
			}
		}		
	});

	// Form validation
	$('form')
		.on('blur', 'input[required], input.optional, select.required', validator.checkField)
		.on('change', 'select.required  input[required]', validator.checkField)
		.on('keypress', 'input[required][pattern]', validator.keypress);

	$('.multi.required').on('keyup blur', 'input', function() {
		validator.checkField.apply($(this).siblings().last()[0]);
	});

	// Submit event
	$('#order-form').submit(function(e) {
		e.preventDefault();
		var canSubmit = true;

		if (!validator.checkAll($(this))) {
			canSubmit = false;
		}
		
		if (!confi1){
			new PNotify({
				title: "Chú ý",
				text: "Nhân viên không tồn tại. Nhấn F2 để chọn từ danh sách.",
				type: "error",
				hide: true,
				delay: 3000,
				styling: "bootstrap3"
			});
			canSubmit = false;
		}
		
		if (!confi2){
			new PNotify({
				title: "Chú ý",
				text: "Khách hàng không tồn tại. Nhấn F2 để chọn từ danh sách.",
				type: "error",
				hide: true,
				delay: 3000,
				styling: "bootstrap3"
			});
			canSubmit = false;
		}
		
		if (canSubmit){
			// Send ajax to add ReturnedGoods detail
			$.ajax({
				url: ctx + '/salesOrders/api/details/' + orderNo.val(),
				method: 'post',
				contentType: 'json',
				data: JSON.stringify(details),
				beforeSend: function(xhr) {
					xhr.setRequestHeader(header, token);
					xhr.setRequestHeader("Accept", "application/json");
					xhr.setRequestHeader("Content-Type", "application/json");
				}
			});
			$(this).attr('action', $(this).attr('action') + '/' + salespersonId.val() + '/' + customerId.val());
			this.submit();
		}
		return false;
	});
});