<%@include file="templates/header.jsp"%>

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<%@include file="templates/drawer.jsp"%>
			<%@include file="templates/navbar.jsp"%>
			<img width="100%" height="110%" src="<spring:url value="/resources/images/background.jpg"/>" /> 
			<!-- page content -->
<!-- 			<div class="right_col" role="main"></div> -->
			<!-- /page content -->
		</div>
		<%@include file="templates/footer.jsp"%>
</body>