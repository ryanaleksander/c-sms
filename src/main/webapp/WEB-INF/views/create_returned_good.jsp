<%@include file="templates/header.jsp"%>
<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<%@include file="templates/drawer.jsp"%>
			<%@include file="templates/navbar.jsp"%>

			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
						<div class="title_left">
							<h3>Tạo mới giao dịch trả hàng</h3>
						</div>
					</div>
					<div class="clearfix"></div>

					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_content">							
									<form:form
										action="${pageContext.request.contextPath}/create_returned_good"
										class="form-horizontal form-label-left"
										commandName="salesOrder" method="post" id="order-form"
										novalidate="true">
										<span class="section">Thông tin cơ bản</span>

										<div class="item form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12"
												for="order-no">Số giao dịch <span class="required">*</span>
											</label>
											<div class="col-md-6 col-sm-6 col-xs-12">
											<c:set var="rowno" value="${param.radioButton - 1}" />
												<form:input id="order-no"
													class="form-control col-md-7 col-xs-12" name="orderNo"
													path="orderNo" required="required" type="text"
													value ="${salesOrders[rowno].orderNo}"
													autofocus="true" data-toggle="tooltip" data-placement="top"
													title="Nhấp F2 chọn từ danh sách" />
											</div>
										</div>
										<div class="item form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12"
												for="order-date">Ngày giao dịch <span
												class="required">*</span>
											</label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<form:input type="text" id="order-date" name="orderDate"
													path="orderDate" required="required"
													class="datepicker form-control col-md-7 col-xs-12" />
											</div>
										</div>
										<div class="item form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12"
												for="sales-personId">Mã nhân viên <span class="required">*</span>
											</label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<input id="sales-personId"
													class="form-control col-md-7 col-xs-12"
													 type="text" required="required"
													autofocus="true" data-toggle="tooltip" data-placement="top"
													title="Nhấp F2 chọn từ danh sách" />
											</div>
										</div>
										<div class="item form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12"
												for="sales-personName">Tên nhân viên
											</label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<input id="sales-personName"
													class="form-control col-md-7 col-xs-12" 
													 type="text"/> 
											</div>
										</div>
										<div class="item form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12"
												for="customer-id">Mã khách hàng <span class="required">*</span>
											</label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<input id="customer-id"
													class="form-control col-md-7 col-xs-12"
													 type="text" required="required"
													autofocus="true" data-toggle="tooltip" data-placement="top"
													title="Nhấp F2 chọn từ danh sách" />
											</div>
										</div>
										<div class="item form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12"
												for="customer-name">Tên khách hàng
											</label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<input id="customer-name"
													class="form-control col-md-7 col-xs-12" 
													 type="text"/>
											</div>
										</div>
										<div class="item form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12"
												for="description">Mô tả
											</label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<form:textarea id="description"
													class="form-control col-md-7 col-xs-12" name="description"
													path="description"  type="text"
													autofocus="true"  />
											</div>
										</div>
										
										<span class="section">Thông tin chi tiết</span>
										<div class="item row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="x_panel">
													<div class="x_title">
														<button data-toggle="popover" data-placement="right"
															class="btn btn-default nav navbar-left"
															id="add-inventory" type="button"
															style="padding-left: 10px;">Thêm mặt hàng</button>
														<ul class="nav navbar-right panel_toolbox">
															<li><a class="collapse-link"><i
																	class="fa fa-chevron-up"></i></a></li>
														</ul>
														<div class="clearfix"></div>
													</div>
													<div class="x_content">
														<table id="datatable"
															class="table table-striped table-bordered">
															<thead>
																<tr>
																	<th>Mặt hàng</th>
																	<th>Tên hàng</th>
																	<th>Số lượng</th>
																	<th>Đơn giá</th>
																	<th>Đơn vị tính</th>
																	<th>Chiết khấu</th>
																	<th>VAT</th>
																	<th>Thành tiền</th>
																</tr>
															</thead>

															<tbody>

															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
										<span class="section">Thông tin tổng hợp</span>
										<div class="item form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12"
												for="order-disc">Chiết khấu </label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<form:input type="number" id="order-disc" name="orderDisc"
													path="orderDisc" readonly="true"
													class="form-control col-md-7 col-xs-12" />
											</div>
										</div>
										<div class="item form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12"
												for="tax-amt">Thuế GTGT </label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<form:input type="number" id="tax-amt" path="taxAmt"
													name="taxAmt" class="form-control col-md-7 col-xs-12"
													readonly="true" />
											</div>
										</div>

										<div class="item form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12"
												for="total-amt">Thành tiền </label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<form:input type="number" id="total-amt" path="totalAmt"
													name="totalAmt" class="form-control col-md-7 col-xs-12"
													readonly="true" />
											</div>
										</div>
										<input type="hidden" name="${_csrf.parameterName}"
											value="${_csrf.token}" />
										<div class="ln_solid"></div>
										<div class="form-group">
											<div class="col-md-1 col-md-offset-3">
												<button id="submit-btn" type="submit" class="btn btn-success"
												 > Lưu </button>
											</div>
											<div class="col-md-1">
											<a href="<c:url value="/" />" title="" class="product-image">
												<button id="back" type="button" class="btn btn-default">Trở về
												</button>
											</a>
										</div>
										</div>
										<input type="hidden" name="${_csrf.parameterName}"
											value="${_csrf.token}" />
									</form:form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="popover-head" class="hide">Thông tin mặt hàng</div>
	<div id="popover-content" class="hide">
		<form class="form-horizontal form-label-left" novalidate>
			<div class="item form-group">
				<label class="control-label" for="invt-id">Mã mặt hàng</label> <br />
				<select id="invt-id" class="form-control" style="width: 200px">
					<c:forEach items="${inventories}" var="inventory">
						<option value="${inventory.invtId}">${inventory.invtId}-
							${inventory.invtName}</option>
					</c:forEach>
				</select>
			</div>
			<div class="item form-group">
				<label for="qty" class="control-label">Số lượng</label> <input
					class="form-control" type="number" id="qty" />
			</div>
			<div class="item form-group">
				<label for="prom" class="control-label">Chiết khấu</label> <input
					class="form-control" type="number" id="prom" />
			</div>
		</form>
		<button type="button" class="btn btn-primary" id="inventory-submit">
			Xác nhận</button>
	</div>

	<div id="order-modal" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog" style="width: 1000px; z-index: 9999">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Chọn một giao dịch</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<table id="modal-datatable" style="width: 100%;"
								class="table table-striped display">
								<thead>
									<tr>
										<th>Số giao dịch</th>
										<th>Ngày giao dịch</th>
										<th>Mã nhân viên</th>
										<th>Tên nhân viên</th>
										<th>Mã khách hàng</th>
										<th>Tên khách hàng</th>
										<th>Thành tiền</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${salesOrders}" var="salesOrder">
										<c:if
											test="${salesOrder.getInvoiceType().getInvoiceType() == 'NM'}">
											<tr>
												<td>${salesOrder.orderNo}</td>
												<td>${salesOrder.orderDate}</td>
												<td>${salesOrder.getSalesPersonId().getSalesPersonId()}</td>
												<td>${salesOrder.getSalesPersonId().getSalesPersonName()}</td>
												<td>${salesOrder.getCustomerId().getCustId()}</td>
												<td>${salesOrder.getCustomerId().getCustomerName()}</td>
												<td>${salesOrder.totalAmt}</td>
												<td>
													<button id="selectOrder" type="button"
														class="btn btn-primary btn-xs select-btn">Chọn
													</button>
												</td>
											</tr>
										</c:if>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
				</div>
			</div>

		</div>
	</div>
	<div id="salesPerson-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width:1000px;z-index:9999">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Chọn một nhân viên</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <table id="modal-datatable" style="width: 100%;" class="table table-striped display">
                            <thead>
                            <tr>
                                <th>Mã nhân viên</th>
								<th>Tên nhân viên</th>
								<th>Địa chỉ</th>
								<th>Trạng thái</th>
								<th>Mô tả</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${salesPerson}" var="salesPerson">
                           
                                <tr>
                                    <td>${salesPerson.getSalesPersonId()}</td>
									<td>${salesPerson.getSalesPersonName()}</td>
									<td>${salesPerson.getAddress()}</td>
									<td>${salesPerson.getStatus().getStatusName()}</td>
									<td>${salesPerson.getDescription()}</td>
									
                                    <td>
                                        <button id="selectPerson" type="button"
                                                class="btn btn-primary btn-xs select-btn">
                                            Chọn
                                        </button>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>
<div id="customer-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width:1000px;z-index:9999">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Chọn một khách hàng</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <table id="modal-datatable" style="width: 100%;" class="table table-striped display">
                            <thead>
                            <tr>
                                <th>Mã khách hàng</th>
								<th>Tên khách hàng</th>
								<th>Địa chỉ</th>
								<th>Trạng thái</th>
								<th>Điện thoại</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${customer}" var="customer">
                           
                                <tr>
                                    <td>${customer.getCustId()}</td>
									<td>${customer.getCustomerName()}</td>
									<td>${customer.getAddress()}</td>
									<td>${customer.getStatus().getStatusName()}</td>
									<td>${customer.getPhone()}</td>
									
                                    <td>
                                        <button id="selectCustomer" type="button"
                                                class="btn btn-primary btn-xs select-btn">
                                            Chọn
                                        </button>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>

	<%@include file="templates/footer.jsp"%>
	<script>
		var ctx = "${pageContext.request.contextPath}";
	</script>
	<script
		src="<spring:url value="/resources/js/create_returned_good.js"/>"></script>
</body>