<div class="col-md-3 left_col menu_fixed" style="z-index: 10">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="<c:url value="/" />" class="site_title"><i class="fa fa-paw"></i> <span>C-SMS</span></a>
        </div>
        <div class="clearfix"></div>
        <br />
        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>Các chức năng chính</h3>
                <ul class="nav side-menu">
                    <li><a href="<c:url value="/" />"><i class="fa fa-home"></i> Trang chủ </a></li>
                    <li><a><i class="fa fa-edit"></i> Chuyển tiền <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<c:url value="/transaction"/> ">Danh sách phiếu chuyển tiền</a></li>
                            <li><a href="<c:url value="/create_transaction" />">Lập phiếu chuyển tiền</a></li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-money"></i> Hóa đơn mua hàng <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<c:url value="/purchase_orders"/>">Danh sách hóa đơn mua hàng</a></li>
                            <li><a href="<c:url value="/create_purchase_order"/>">Lập hóa đơn mua hàng</a></li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-share-square-o"></i> Trả hàng cho công ty <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<c:url value="/purchase_return"/>">Danh sách giao dịch</a></li>
                            <li><a href="<c:url value="/create_purchase_return"/>">Tạo mới giao dịch</a></li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-shopping-cart"></i> Hóa đơn bán hàng <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<c:url value="/sales_order"/>">Danh sách hóa đơn bán hàng</a></li>
                            <li><a href="<c:url value="/create_sales_order"/>">Lập hóa đơn bán hàng</a></li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-joomla"></i> Trả hàng cho NPP<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<c:url value="/returned_goods"/>">Danh sách giao dịch</a></li>
                            <li><a href="<c:url value="/create_returned_good"/>">Tạo mới giao dịch</a></li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-dashcube "></i> Phiếu xuất kho <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<c:url value="/delivery_orders"/>">Danh sách phiếu xuất kho</a></li>
                            <li><a href="<c:url value="/create_delivery_order"/>">Tạo mới phiếu xuất kho</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->
    </div>
</div>