<!-- footer content -->
<footer>
    <div class="pull-right">
        Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
<!-- jQuery -->
<script src="<spring:url value="/resources/vendors/jquery/dist/jquery.min.js"/>"></script>
<!-- Bootstrap -->
<script src="<spring:url value="/resources/vendors/bootstrap/dist/js/bootstrap.min.js"/>"></script>
<!-- FastClick -->
<script src="<spring:url value="/resources/vendors/fastclick/lib/fastclick.js"/>"></script>
<!-- iCheck -->
<script src="<spring:url value="/resources/vendors/iCheck/icheck.min.js"/>"></script>
<!-- bootstrap-daterangepicker -->
<script src="<spring:url value="/resources/js/moment/moment.min.js"/>"></script>
<script src="<spring:url value="/resources/js/datepicker/daterangepicker.js"/>"></script>
<!-- datatabls -->
<script src="<spring:url value="/resources/vendors/datatables.net/js/jquery.dataTables.min.js"/>"></script>
<script src="<spring:url value="/resources/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"/>"></script>
<script src="<spring:url value="/resources/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"/>"></script>
<script src="<spring:url value="/resources/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"/>"></script>
<script src="<spring:url value="/resources/vendors/datatables.net-buttons/js/buttons.flash.min.js"/>"></script>
<script src="<spring:url value="/resources/vendors/datatables.net-buttons/js/buttons.html5.min.js"/>"></script>
<script src="<spring:url value="/resources/vendors/datatables.net-buttons/js/buttons.print.min.js"/>"></script>
<script src="<spring:url value="/resources/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"/>"></script>
<script src="<spring:url value="/resources/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"/>"></script>
<script src="<spring:url value="/resources/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"/>"></script>
<script src="<spring:url value="/resources/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"/>"></script>
<script src="<spring:url value="/resources/vendors/datatables.net-scroller/js/datatables.scroller.min.js"/>"></script>
<!-- validator -->
<script src="<spring:url value="/resources/vendors/validator/validator.min.js"/>"></script>
<!-- PNotify -->
<script src="<spring:url value="/resources/vendors/pnotify/dist/pnotify.js"/>"></script>
<script src="<spring:url value="/resources/vendors/pnotify/dist/pnotify.buttons.js"/>"></script>
<script src="<spring:url value="/resources/vendors/pnotify/dist/pnotify.nonblock.js"/>"></script>

<script src="<spring:url value="/resources/js/custom.min.js"/>"></script>

