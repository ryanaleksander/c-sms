<%@include file="templates/header.jsp" %>
<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <%@include file="templates/drawer.jsp" %>
        <%@include file="templates/navbar.jsp" %>

        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Lập phiếu chuyển tiền</h3>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_content">

                                <form:form action="${pageContext.request.contextPath}/create_transaction"
                                           class="form-horizontal form-label-left" commandName="moneyTransaction"
                                           method="post" acceptCharset="UTF-8"
                                           id="transaction-form" novalidate="true">
                                    <span class="section">Thông tin phiếu chuyển tiền</span>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="trans-id">Mã
                                            chuyển tiền <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <form:input id="trans-id" class="form-control col-md-7 col-xs-12"
                                                        name="transId" path="transId" required="required" type="text"
                                                        autofocus="true" data-toggle="tooltip"
                                                        data-placement="top" title="Nhấn F2 để chọn từ danh sách"/>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="trans-date">Ngày
                                            chuyển tiền <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <form:input type="text" id="trans-date" name="transDate" path="transDate"
                                                        required="required"
                                                        class="datepicker form-control col-md-7 col-xs-12"/>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="trans-amt">Số
                                            lượng chuyển <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <form:input type="number" id="trans-amt" name="transAmt" path="transAmt"
                                                        required="required" class="form-control col-md-7 col-xs-12"/>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Ghi
                                            chú
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <form:textarea id="description" path="description" name="description"
                                                           class="form-control col-md-7 col-xs-12"/>
                                        </div>
                                    </div>
                                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-3">
                                            <button id="submit-btn" type="submit" class="btn btn-success">Xác nhận</button>
                                        </div>
                                    </div>
                                </form:form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="transaction-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width:1000px;z-index:9999">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Chọn một hóa đơn</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <table id="modal-datatable" style="width: 100%;" class="table table-striped display">
                            <thead>
                            <tr>
                                <th>Mã chuyển tiền</th>
                                <th>Ngày chuyển tiền</th>
                                <th>Số lượng chuyển</th>
                                <th>Ghi chú</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${transactions}" var="transaction">
                                <tr>
                                    <td>${transaction.transId}</td>
                                    <td>${transaction.transDate}</td>
                                    <td>${transaction.transAmt}</td>
                                    <td>${transaction.description}</td>
                                    <td>
                                        <button type="button"
                                                class="btn btn-primary btn-xs select-btn">
                                            Chọn
                                        </button>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>
<%@include file="templates/footer.jsp" %>
<script>
    var ctx = "${pageContext.request.contextPath}";
</script>
<script src="<spring:url value="/resources/js/create_transaction.js"/>"></script>
</body>