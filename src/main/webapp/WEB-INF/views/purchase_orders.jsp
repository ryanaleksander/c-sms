<%@include file="templates/header.jsp"%>
<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <%@include file="templates/drawer.jsp"%>
        <%@include file="templates/navbar.jsp"%>
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Danh sách hóa đơn mua hàng</h3>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_content">
                                <table id="datatable" class="table table-striped table-bordered display">
                                    <thead>
                                    <tr>
                                        <th>Số hóa đơn</th>
                                        <th>Ngày hóa đơn</th>
                                        <th>Chiết khấu</th>
                                        <th>Thuế GTGT</th>
                                        <th>Thành tiền</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${purchaseOrders}" var="purchaseOrder">
                                        <tr>
                                            <td>${purchaseOrder.orderNo}</td>
                                            <td>${purchaseOrder.orderDate}</td>
                                            <td>${purchaseOrder.discAmt}</td>
                                            <td>${purchaseOrder.taxAmt}</td>
                                            <td>${purchaseOrder.totalAmt}</td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%@include file="templates/footer.jsp"%>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#datatable').DataTable({
            "language": {
                "emptyTable": "Không có dữ liệu"
            }
        });
    });
</script>
<!-- /Datatables -->
</body>
