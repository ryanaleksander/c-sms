<%@include file="templates/header.jsp" %>
<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <%@include file="templates/drawer.jsp" %>
        <%@include file="templates/navbar.jsp" %>

        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Tạo giao dịch trả hàng cho công ty</h3>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_content">
                                <form:form action="${pageContext.request.contextPath}/create_purchase_return"
                                           class="form-horizontal form-label-left" commandName="purchaseOrder"
                                           method="post" id="order-form"
                                           novalidate="true">
                                    <span class="section">Thông tin cơ bản</span>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="order-no">Số giao
                                            dịch
                                            <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <form:input id="order-no" class="form-control col-md-7 col-xs-12"
                                                        name="orderNo" path="orderNo" required="required"
                                                        type="text" autofocus="true"
                                                        data-toggle="tooltip"
                                                        data-placement="top"
                                                        title="Nhấn F2 để chọn từ danh sách"
                                            />
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="order-date">Ngày
                                            giao
                                            dịch <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <form:input type="text" id="order-date" name="orderDate" path="orderDate"
                                                        required="required"
                                                        class="datepicker form-control col-md-7 col-xs-12"/>
                                        </div>
                                    </div>
                                    <span class="section">Thông tin chi tiết</span>
                                    <div class="item row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="x_panel">
                                                <div class="x_title">
                                                    <button data-toggle="popover" data-placement="right"
                                                            class="btn btn-default nav navbar-left"
                                                            id="add-inventory" type="button"
                                                            style="padding-left:10px;">
                                                        Thêm mặt hàng
                                                    </button>
                                                    <ul class="nav navbar-right panel_toolbox">
                                                        <li><a class="collapse-link"><i
                                                                class="fa fa-chevron-up"></i></a>
                                                        </li>
                                                    </ul>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="x_content">
                                                    <table id="datatable" class="table table-striped table-bordered">
                                                        <thead>
                                                        <tr>
                                                            <th>Mã hàng</th>
                                                            <th>Tên hàng</th>
                                                            <th>Số lượng</th>
                                                            <th>Đơn giá</th>
                                                            <th>Đơn vị tính</th>
                                                            <th>Chiết khấu</th>
                                                            <th>Thành tiền</th>
                                                        </tr>
                                                        </thead>

                                                        <tbody>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <span class="section">Thông tin tổng hợp</span>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="disc-amt">Chiết
                                            khấu
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <form:input type="number" id="disc-amt" name="discAmt" path="discAmt"
                                                        readonly="true" class="form-control col-md-7 col-xs-12"/>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tax-amt">Thuế GTGT
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <form:input type="number" id="tax-amt" path="taxAmt" name="taxAmt"
                                                        class="form-control col-md-7 col-xs-12" readonly="true"/>
                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="total-amt">Thành
                                            tiền
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <form:input type="number" id="total-amt" path="totalAmt" name="totalAmt"
                                                        class="form-control col-md-7 col-xs-12" readonly="true"/>
                                        </div>
                                    </div>
                                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-3">
                                            <input id="submit-btn" type="submit" class="btn btn-success"
                                                   value="Xác nhận"/>
                                        </div>
                                    </div>
                                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                </form:form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="popover-head" class="hide">Thông tin mặt hàng</div>
<div id="popover-content" class="hide">
    <form class="form-horizontal form-label-left" novalidate>
        <div class="item form-group">
            <label class="control-label" for="invt-id">Mã mặt hàng</label>
            <br/>
            <select id="invt-id" class="form-control" style="width: 200px">
                <c:forEach items="${inventories}" var="inventory">
                    <option value="${inventory.invtId}">${inventory.invtId} - ${inventory.invtName}</option>
                </c:forEach>
            </select>
        </div>
        <div class="item form-group">
            <label for="qty" class="control-label">Số lượng</label>
            <input class="form-control" type="number" id="qty"/>
        </div>
        <div class="item form-group">
            <label for="prom" class="control-label">Chiết khấu</label>
            <input class="form-control" type="number" id="prom"/>
        </div>
    </form>
    <button type="button" class="btn btn-primary" id="inventory-submit">
        Xác nhận
    </button>
</div>

<div id="order-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width:1000px;z-index:9999">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Chọn một giao dịch</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <table id="modal-datatable" style="width: 100%;" class="table table-striped display">
                            <thead>
                            <tr>
                                <th>Số giao dịch</th>
                                <th>Ngày giao dịch</th>
                                <th>Chiết khấu</th>
                                <th>Thuế GTGT</th>
                                <th>Thành tiền</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${purchaseOrders}" var="purchaseOrder">
                                <tr>
                                    <td>${purchaseOrder.orderNo}</td>
                                    <td>${purchaseOrder.orderDate}</td>
                                    <td>${purchaseOrder.discAmt}</td>
                                    <td>${purchaseOrder.taxAmt}</td>
                                    <td>${purchaseOrder.totalAmt}</td>
                                    <td>
                                        <button type="button"
                                                class="btn btn-primary btn-xs select-btn">
                                            Chọn
                                        </button>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>

    </div>
</div>
<%@include file="templates/footer.jsp" %>
<script>
    var ctx = "${pageContext.request.contextPath}";
</script>
<script src="<spring:url value="/resources/js/create_purchase_return.js"/>"></script>
</body>