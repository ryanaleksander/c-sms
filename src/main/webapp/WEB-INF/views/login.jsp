<%@include file="templates/header.jsp"%>
<link href="<spring:url value="/resources/css/login.css"/>" rel="stylesheet"/>
<div class="container">
    <div class="card card-container" style="text-align: center">
        <h3 style="margin: auto">Xin chào!</h3>
        <h2 style="margin: auto">Vui lòng đăng nhập để tiếp tục</h2>
        <p id="profile-name" class="profile-name-card"></p>
        <form class="form-signin" action="${pageContext.request.contextPath}/login" method="post">
            <span id="reauth-email" class="reauth-email"></span>
            <input name="username" type="text" id="inputEmail" class="form-control" placeholder="Username" required autofocus>
            <input name="password" type="password" id="inputPassword" class="form-control" placeholder="Password" required>
            <input class="btn btn-lg btn-primary btn-block btn-signin" type="submit" value="Đăng nhập" />
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        </form><!-- /form -->
    </div><!-- /card-container -->
</div><!-- /container -->
<script src="<spring:url value="/resources/vendors/jquery/dist/jquery.min.js"/>"></script>
<script src="<spring:url value="/resources/vendors/pnotify/dist/pnotify.js"/>"></script>
<script src="<spring:url value="/resources/vendors/pnotify/dist/pnotify.buttons.js"/>"></script>
<script src="<spring:url value="/resources/vendors/pnotify/dist/pnotify.nonblock.js"/>"></script>
<!-- Bootstrap -->
<script src="<spring:url value="/resources/vendors/bootstrap/dist/js/bootstrap.min.js"/>"></script>
<c:if test="${not empty msg}">
    <script>
        $(document).ready(function() {
            new PNotify({
                title: "Đăng xuất",
                text: '${msg}',
                type: "success",
                hide: false,
                styling: "bootstrap3"
            });
        });
    </script>
</c:if>
<c:if test="${not empty error}">
    <script>
        $(document).ready(function() {
            new PNotify({
                title: "Lỗi",
                text: '${error}',
                type: "error",
                hide: false,
                styling: "bootstrap3"
            });
        });
    </script>
</c:if>

