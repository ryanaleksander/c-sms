<%@include file="templates/header.jsp"%>
<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<%@include file="templates/drawer.jsp"%>
			<%@include file="templates/navbar.jsp"%>

			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
						<div class="title_left">
							<h3>Danh sách giao dịch trả hàng</h3>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_content">
								
									<form action="<c:url value="/create_returned_good"/>">
										<table id="datatable"
											class="table table-striped table-bordered display">
											<thead>
												<tr>
													<th></th>
													<th>Số giao dịch</th>
													<th>Ngày giao dịch</th>
													<th>Mã nhân viên</th>
													<th>Tên nhân viên</th>
													<th>Mã khách hàng</th>
													<th>Tên khách hàng</th>
													<th>Thành tiền</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${returnedGoods}" var="returnedGoods" varStatus="status">
												<c:if
														test="${returnedGoods.getInvoiceType().getInvoiceType() == 'NM'}">
														<tr>
															<td><input id="radio-check" type="radio" name="radioButton"
																value="${status.count}"></td>
															<td>${returnedGoods.orderNo}</td>
															<td>${returnedGoods.orderDate}</td>
															<td>${returnedGoods.getSalesPersonId().getSalesPersonId()}</td>
															<td>${returnedGoods.getSalesPersonId().getSalesPersonName()}</td>
															<td>${returnedGoods.getCustomerId().getCustId()}</td>
															<td>${returnedGoods.getCustomerId().getCustomerName()}</td>
															<td>${returnedGoods.totalAmt}</td>
														</tr>
													</c:if>
												</c:forEach>
											</tbody>
										</table>
										</br>
										<div class="col-md-1 col-md-offset-10">
											<button id="submit-radiobtn" type="submit" name="reserveButton"
												class="btn btn-success">Chọn</button>
										</div>
										<div class="col-md-1">
											<a href="<c:url value="/" />" title="" class="product-image">
												<button id="back" type="button" class="btn btn-default">Trở
													về</button>
											</a>
										</div>
									</form>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
			<%@include file="templates/footer.jsp"%>
		</div>
	</div>
	<script>
	 $(document).ready(function() {
		var submitRadio = $('#submit-radiobtn');
		submitRadio.attr('disabled', true);
		$('body').on("click", "#radio-check", function() {
			submitRadio.attr('disabled', false);
		});
	 });
	</script>
	<script>
    $(document).ready(function() {
        $('#datatable').DataTable({
            "language": {
                "emptyTable": "Không có dữ liệu"
            }
        });
    });
</script>
</body>
